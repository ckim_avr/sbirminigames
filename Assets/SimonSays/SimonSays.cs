﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;
using SBIR.Utils;
using System.Linq;
using GTools.Utils.Extensions;

namespace SBIR.MiniGames
{
	public class SimonSays : MiniGame
	{
		public AudioClip errorSound;

		private AudioSource source;

		private class SimonSaysCrystal
		{
			public int index;
			public bool playSound = false;
		}

		//public int maxSequenceLength = 8;

		public override event Action GameStarted;
		public override event Action GameEnded;

		public override event Action RoundComplete;

		public CrystalMaterialController[] crystals;

		private CrystalMaterialController currUserChoice = null;

		private bool userInputAllowed = false;

		private int idx = 1;

		//private Summary summary;
		//private Summary.RoundAttributes currRoundSummary;

		private SimonSaysCrystal[] sequence;
		private SimonSaysCrystal[] userSequence;
		private Coroutine waveRoutine = null;
		private Coroutine sequenceRoutine = null;
		private Coroutine userInputRoutine = null;

		private void Awake()
		{
			source = GetComponent<AudioSource>();
		}

		private void OnEnable()
		{
			IntroTimeline.gameObject.SetActive(false);
			OutroTimeline.gameObject.SetActive(false);
			foreach (var round in RoundTimelines)
			{
				round.gameObject.SetActive(false);
			}
			StartCoroutine(_Intro());
			MaxRounds = 1;
		}

		[ContextMenu("Game Start")]
		public override void GameStart()
		{
			Debug.Log("game start");
			CreateNewSequence();
			Subscribe();
			//call action so MiniGameManager can call PlayRound()
			if (GameStarted != null)
				GameStarted();
		}
		[ContextMenu("New Sequence")]
		public void CreateNewSequence()
		{
			float round2LitPropability = 0.6f;
			float round3LitPropability = 0.4f;
			sequence = new SimonSaysCrystal[100];
			for (int i = 0; i < sequence.Length; i++)
			{
				sequence[i] = new SimonSaysCrystal();
				var currSeqObj = sequence[i];
				currSeqObj.index = UnityEngine.Random.Range(0, crystals.Length);
				// Assign some items in squence to play sounds
				if (CurrentRound == 0)
				{
					currSeqObj.playSound = true;
				}
				else if (CurrentRound == 1)
				{
					currSeqObj.playSound = UnityEngine.Random.Range(0f, 1f) < round2LitPropability ? true : false;
				}
				else if (CurrentRound == 2)
				{
					currSeqObj.playSound = UnityEngine.Random.Range(0f, 1f) < round3LitPropability ? true : false;
				}
			}
			sequence[0].playSound = true;
			// Generate user sequence and validate
			System.Collections.Generic.List<int> indices = new System.Collections.Generic.List<int>();
			for (int i = 0; i < crystals.Length; i++)
			{
				indices.Add(i);
			}
			if (CurrentRound == 0)
			{
				userSequence = sequence.Where(sO => sO.playSound == true).ToArray();
			}
			else if (CurrentRound == 1)
			{
				ValidateSequence(ref sequence, true);
				userSequence = sequence.Where(sO => sO.playSound == true).ToArray();
			}
			else if (CurrentRound == 2)
			{
				sequence[0].playSound = false;
				ValidateSequence(ref sequence, false);
				userSequence = sequence.Where(sO => sO.playSound == false).ToArray();
			}
		}

		private void ValidateSequence(ref SimonSaysCrystal[] sequence, bool sequenceStartsWithSound)
		{
			if (sequence.Length == 0)
			{
				Debug.LogError("Sequence is empty!");
				return;
			}

			int currValCount = 0;
			int maxRepeat = 1;
			bool prevVal = sequence[0].playSound;
			for (int i = 1; i < sequence.Length; i++)
			{
				var sO = sequence[i];
				if(sO.playSound == prevVal)
				{
					if (currValCount >= maxRepeat)
					{
						currValCount = 0;
						sO.playSound = !sO.playSound;
					}
					currValCount++;
				}
				else
				{
					currValCount = 0;
				}

				prevVal = sO.playSound;
			}
		}

		void Subscribe()
		{
			foreach (var crystal in crystals)
			{
				crystal.SimonSaysTap += CrystalTapped;
			}
		}

		public override void GameEnd()
		{
			StartCoroutine(_Outro());
		}

		void UnSubscribe()
		{
			foreach (var crystal in crystals)
			{
				crystal.SimonSaysTap -= CrystalTapped;
			}
		}

		public override IEnumerator _Intro()
		{
			if (MiniGameManager.Instance != null)
			{
				MiniGameManager.Instance.GameUsesProgressBar = false;
			}
			Debug.Log("start intro");
			IntroTimeline.gameObject.SetActive(true);
			IntroTimeline.extrapolationMode = DirectorWrapMode.Hold;
			IntroTimeline.Play();
			while (IntroTimeline.time < IntroTimeline.duration)
			{
				yield return null;
			}
			//do intro things
			GameStart();
			IntroTimeline.Stop();
			IntroTimeline.gameObject.SetActive(false);
		}

		public override IEnumerator _Outro()
		{
			Debug.Log("start outro");
			OutroTimeline.gameObject.SetActive(true);
			OutroTimeline.extrapolationMode = DirectorWrapMode.Hold;
			OutroTimeline.Play();
			while (OutroTimeline.time < OutroTimeline.duration)
			{
				yield return null;
			}
			OutroTimeline.Stop();
			OutroTimeline.gameObject.SetActive(false);
			if (GameEnded != null)
				GameEnded();
		}
		public override void PlayRound()
		{
			Debug.Log("play round");
			if (CurrentRound > 2)
			{
				GameEnd();
				return;
			}
			StartCoroutine(_Round(CurrentRound));
		}


		IEnumerator _PlaySequence(int index, System.Action<bool> result)
		{
			Debug.Log("Simon Says");
			yield return new WaitForEndOfFrame();
			for (int i = 0; i < index; i++)
			{
				var seqObj = sequence[i];
				var crystal = crystals[seqObj.index];
				bool playSound = seqObj.playSound;
				yield return new WaitForSeconds(0.1f);
				crystal.SetState(CrystalLitState.Lit);
				if (playSound) crystal.PlaySound();
				yield return new WaitForSeconds(0.5f);
				crystal.SetState(CrystalLitState.Unlit);
			}
			yield return new WaitForSeconds(0.2f);
			result(true);
			yield return null;
		}


		IEnumerator _UserInput(int index, System.Action<bool> result)
		{
			Debug.Log("User Input");
			userInputAllowed = true;
			var startTime = Time.time;
			for (int i = 0; i < index; i++)
			{
				Debug.LogFormat("Current index: {0}", i);
				yield return new WaitUntil(() => currUserChoice != null);

				if (currUserChoice == crystals[userSequence[i].index])
				{
					if (i == 0)
					{
						var t = Time.time - startTime;
					}
					currUserChoice = null;
					var crystal = crystals[userSequence[i].index];
					crystal.SetState(CrystalLitState.Lit);
					crystal.PlaySound();
					yield return new WaitForSeconds(0.1f);
					crystal.SetState(CrystalLitState.Unlit);
					Debug.Log("Correct");
				}
				else
				{
					currUserChoice = null;
					Debug.Log("InCorrect");
					userInputAllowed = false;
					source.PlayOneShot(errorSound);
					yield return new WaitForSeconds(0.2f);
					result(false);
					yield break;
				}
			}
			yield return new WaitForSeconds(0.2f);
			userInputAllowed = false;
			var spanTime = Time.time - startTime;
			result(true);
		}

		void CrystalTapped(CrystalMaterialController obj)
		{
			if (userInputAllowed)
			{
				currUserChoice = obj;
			}
		}

		public IEnumerator _Wave(int index)
		{
			//get handle to a simon says sequence for this round index
			var simonSays = new WaitForCallback<bool>(
				simonHandle => sequenceRoutine = StartCoroutine(_PlaySequence(index, callback => simonHandle(callback))));
			yield return simonSays;

			Debug.LogErrorFormat("Sequence:\n\t{0}", string.Join(", ", sequence.Select(x => x.index.ToString()).ToArray()));

			// Find how many items the user has to choose correctly to continue
			int userIndex = index;
			if (CurrentRound == 1)
				userIndex = sequence.Take(index).Where(val => val.playSound == true).Count();
			else if (CurrentRound == 2)
				userIndex = sequence.Take(index).Where(val => val.playSound == false).Count();

			//Debug.LogErrorFormat("User Sequence:\n\t{0}", string.Join(", ", userSequence.Select(x => x.index.ToString()).ToArray()));
			//Debug.LogErrorFormat("User Index: {0}", userIndex.ToString() );

			var userInput = new WaitForCallback<bool>(
				userHandle => userInputRoutine = StartCoroutine(_UserInput(userIndex, callback => userHandle(callback))));
			yield return userInput;
			userInputRoutine = null;

			//if user did not do the sequence correctly, restart round
			if (userInput.Result == false)
			{
				// Redo same sequence
				yield return new WaitForSeconds(0.5f);
				waveRoutine = StartCoroutine(_Wave(index));
				yield break;
			}
			else
			{
				// Continue to next sequence
				yield return new WaitForSeconds(1.5f);
				waveRoutine = StartCoroutine (_Wave(++index));
				yield break;
			}
		}

		public void StopCurrentRound()
		{
			if (waveRoutine != null)
				StopCoroutine(waveRoutine);
			if (sequenceRoutine != null)
				StopCoroutine(sequenceRoutine);
			if (userInputRoutine != null)
				StopCoroutine(userInputRoutine);
			foreach (var crystal in crystals)
			{
				crystal.SetState(CrystalLitState.Unlit);
			}
		}

		public override IEnumerator _Round(int index)
		{

			RoundTimelines[index].gameObject.SetActive(true);
			RoundTimelines[index].Play();
			yield return new WaitForSeconds((float)RoundTimelines[index].duration);
			Debug.LogWarning("New routine");
			waveRoutine = StartCoroutine(_Wave(1));
			yield return new WaitForSeconds(60f * 1f);
			RoundTimelines[index].Stop();
			RoundTimelines[index].gameObject.SetActive(false);
			StopCurrentRound();
			CurrentRound++;
			if (RoundComplete != null)
				RoundComplete();
			CreateNewSequence();
		}
	}
}
