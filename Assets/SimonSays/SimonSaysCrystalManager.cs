﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SBIR.Utils;

namespace SBIR.MiniGames
{
	public class SimonSaysCrystalManager : MonoBehaviour
	{

		public CrystalMaterialController crystal1;
		public CrystalMaterialController crystal2;
		public CrystalMaterialController crystal3;
		public CrystalMaterialController crystal4;

		private bool toggle;


		private void Start()
		{
			crystal1.SetState(CrystalLitState.Unlit);
			crystal2.SetState(CrystalLitState.Unlit);
			crystal3.SetState(CrystalLitState.Unlit);
			crystal4.SetState(CrystalLitState.Unlit);
		}

		[ContextMenu("Crystal1 Switch State")]
		void DEBUG001()
		{
			switch (toggle)
			{
				case true:
					crystal1.SetState(CrystalLitState.Unlit);
					break;
				case false:
					crystal1.SetState(CrystalLitState.Lit);
					break;
			}
			toggle = !toggle;
		}
	}
}
