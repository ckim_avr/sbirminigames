﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SomeClass
{
	public static void SomeMethod(string someString, System.Action<string> someCallback)
	{
		someCallback(string.Format("Some string from: {0}", someString));
	}
}


public class ExampleWaitForCallback : MonoBehaviour {

	//without WaitForCallback
	IEnumerator SomeAsyncOperation()
	{
		var done = false;
		var result = default(string);
		SomeClass.SomeMethod("this string", r => { result = r; done = true; });
		while (done == false)
		{
			yield return result;
		}
		Debug.LogFormat("got: {0}", result);
	}

	//with WaitForCallback
	IEnumerator SomeAsyncOperation2()
	{
		var thing = new WaitForCallback<string>(
			done => SomeClass.SomeMethod("this string", text => done(text))
			);
		yield return thing;
		Debug.LogFormat("got: {0}", thing.Result);
	}

}
