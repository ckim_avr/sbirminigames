﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SBIR.Utils;
using SBIR.MiniGames;

namespace SBIR.Utils
{
	public enum CrystalLitState
	{
		Lit = 0,
		Unlit = 1,
	}
}

namespace SBIR.MiniGames
{
	public class CrystalMaterialController : MonoBehaviour, ITappable, IGazeInteractable
	{

		public Material unlitMat;
		public Material litMat;
		public ObjectType objectType;
		public AudioSource audioSource;

		[Range(0f, 1f)]
		private float weight;
		private float lastWeight;
		private Renderer rend;
		private Material currentMat;

		ObjectType IGazeInteractable.ObjectType
		{
			get { return objectType; }
			set { objectType = value; }
		}

		public CrystalLitState currentState;

		public System.Action<CrystalMaterialController> SimonSaysTap;

		public SimonSays simonSays;

		public void Start()
		{
			rend = GetComponent<Renderer>();
			currentMat = new Material(unlitMat);
			if (audioSource == null)
				audioSource = GetComponent<AudioSource>();
			rend.material = currentMat;
			UpdateCurrentState();
		}

		public void UpdateCurrentState()
		{
			rend.material.Lerp(unlitMat, litMat, weight);
		}

		public void SetState(CrystalLitState state)
		{
			currentState = state;
			switch (currentState)
			{
				case CrystalLitState.Lit:
					SetWeight(1f);
					break;
				case CrystalLitState.Unlit:
					SetWeight(0f);
					break;
			}
		}

		public void PlaySound()
		{
			if (audioSource != null)
				audioSource.Play();
			else
				Debug.LogError("Missing AudioSource reference.", this);
		}

		private void Update()
		{
			UpdateCurrentState();
		}

		private void SetWeight(float args)
		{
			weight = args;
		}

		void ITappable.OnTapped()
		{
			Debug.LogFormat("clicked: {0}", this);
			if (SimonSaysTap != null)
			{
				SimonSaysTap(this);
			}
		}

		void IGazeInteractable.OnGazeEnter()
		{

		}

		void IGazeInteractable.OnGazeExit()
		{

		}
	}
}
