﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Timeline;
using UnityEngine.Playables;
#pragma warning disable 0414

namespace SBIR.MiniGames
{
	public abstract class MiniGame : MonoBehaviour
	{
		#region events
		public virtual event Action IntroStarted;
		public virtual event Action GameStarted;
		public virtual event Action GameEnded;
		public virtual event Action OutroStarted;
		public virtual event Action RoundComplete;
		#endregion
		private int _maxRounds = 5;

		private float _points = 0f;
		private float _pointsToWinRound = 100f;

		public ObjectType goalType;

		public float Points
		{ get { return _points; } }

		public float PointsToWinRound
		{ get { return _pointsToWinRound; } }

		private int _currentRound = 0;

		private bool _canAddPoints = false;

		private bool _lastRound = false;

		public bool LastRound
		{
			get { return _lastRound; }
			set { _lastRound = value; }
		}

		private bool _roundDone = false;

		public bool RoundDone
		{
			get { return _roundDone; }
			set { _roundDone = value; }
		}


		public PlayableDirector[] RoundTimelines;

		public PlayableDirector IntroTimeline;

		public PlayableDirector OutroTimeline;

		public bool CanAddPoints
		{
			get { return _canAddPoints; }
			set { _canAddPoints = value; }
		}

		private float _lastTime;
		private float _currTime;

		public virtual int MaxRounds
		{
			get { return _maxRounds; }
			set { _maxRounds = value; }
		}

		private float _rateOfChange;

		public virtual float RateOfChange
		{
			get { return _rateOfChange; }
			set { _rateOfChange = value; }
		}

		public virtual int CurrentRound
		{
			get { return _currentRound; }
			set { _currentRound = value; }
		}

		public abstract IEnumerator _Intro();
		public abstract IEnumerator _Outro();
		public abstract IEnumerator _Round(int index);
		public abstract void GameStart();
		public abstract void PlayRound();
		public abstract void GameEnd();

		public virtual void AddPoints()
		{
			if (false == _canAddPoints) return;
			_points = Mathf.Clamp(_points + RateOfChange, 0, _pointsToWinRound);
			CompletionCheck();
		}

		public virtual void SubtractPoints()
		{
			if (_canAddPoints == false) return;
			_points = Mathf.Clamp(_points - RateOfChange, 0f, _pointsToWinRound);
		}

		protected virtual void CompletionCheck()
		{
			if (_points == _pointsToWinRound)
			{
				if (RoundComplete != null)
				{
					RoundComplete();
				}
			}
		}

		public virtual void ResetPoints()
		{
			_points = 0;
		}
	}
}
