﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SBIR.MiniGames
{
    public enum ObjectType
    {
        Memories = 1,
        Thoughts = 2,
        Emotions = 3,
        Sensations = 4,
        _NUM_OBJECT_TYPES = 5
    }
}