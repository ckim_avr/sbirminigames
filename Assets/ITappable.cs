﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SBIR.Utils
{
	public interface ITappable
	{
		void OnTapped();
	}
}
