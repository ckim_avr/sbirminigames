﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SBIR.MiniGames
{
	public class MiniGameManager : MonoBehaviour
	{
		Color black = new Color(0f, 0f, 0f, 1f);
		Color clear = new Color(0f, 0f, 0f, 0f);

		public System.Action AddedDelegates;
		public System.Action RemovedDelegates;

		public AudioSource roundDoneSound;

		[HideInInspector]
		public bool GameUsesProgressBar = false;

		public float fadeTime = 1f;
		public GameObject fadeSphere;
		private static MiniGameManager _instance;
		public static MiniGameManager Instance
		{
			get
			{
				if (_instance == null)
				{
					Debug.LogError("there is no MiniGameManager in the scene!");
				}
				return _instance;
			}
		}

		public MiniGame[] minigames;

		private Queue<MiniGame> miniGameQueue = new Queue<MiniGame>();

		public MiniGame active;

		private void Awake()
		{
			if (_instance != null && _instance != this)
			{
				DestroyImmediate(this);
				return;
			}

			_instance = this;
			foreach (var game in minigames)
			{
				miniGameQueue.Enqueue(game);
				game.gameObject.SetActive(false);
			}
		}


		private void Start()
		{
			if (miniGameQueue.Count > 0)
			{
				active = miniGameQueue.Dequeue();
				AddDelegates();
				active.gameObject.SetActive(true);
				active.CanAddPoints = true;
			}
			StartCoroutine(_FadeIn(fadeTime));
		}

		void AddDelegates()
		{
			active.GameStarted += ActiveGameStarted;
			active.GameEnded += ActiveGameEnded;
			active.RoundComplete += ActiveGameRoundEnd;
			if (AddedDelegates != null)
				AddedDelegates();
		}

		void RemoveDelegate()
		{
			active.GameStarted -= ActiveGameStarted;
			active.GameEnded -= ActiveGameEnded;
			active.RoundComplete -= ActiveGameRoundEnd;
			if (RemovedDelegates != null)
				RemovedDelegates();
		}


		void ActiveGameStarted()
		{
			Debug.LogFormat("Active Game {0} has Started", active.name);
			active.PlayRound();
		}


		void ActiveGameEnded()
		{
			StartCoroutine(_GameEnded(fadeTime));
		}


		IEnumerator _FadeIn(float t)
		{
			var start = Time.time;
			var elapsed = 0f;
			var normalizedTime = 0f;
			fadeSphere.gameObject.SetActive(true);
			while (elapsed < t)
			{
				elapsed = Time.time - start;
				normalizedTime = Mathf.Clamp(elapsed / t, 0f, 1f);
				fadeSphere.GetComponent<Renderer>().material.color = Color.Lerp(black, clear, normalizedTime);
				yield return null;
			}
			fadeSphere.gameObject.SetActive(false);
		}

		IEnumerator _GameEnded(float t)
		{
			Debug.LogFormat("Active Game {0} has Ended", active.name);
			RemoveDelegate();
			if (miniGameQueue.Count > 0)
			{
				active.gameObject.SetActive(false);
				active.CanAddPoints = false;
				var start = Time.time;
				var elapsed = 0f;
				var normalizedTime = 0f;
				fadeSphere.gameObject.SetActive(true);
				while (elapsed < t)
				{
					elapsed = Time.time - start;
					normalizedTime = Mathf.Clamp(elapsed / t, 0f, 1f);
					fadeSphere.GetComponent<Renderer>().material.color = Color.Lerp(clear, black, normalizedTime);
					yield return null;
				}
				//reset start and elapses for next while loop
				start = Time.time;
				elapsed = 0f;

				//set active mini game while in black
				active = miniGameQueue.Dequeue();
				AddDelegates();
				active.gameObject.SetActive(true);
				active.CanAddPoints = true;
				normalizedTime = 0f;

				while (elapsed < t)
				{
					elapsed = Time.time - start;
					normalizedTime = Mathf.Clamp(elapsed / t, 0f, 1f);
					fadeSphere.GetComponent<Renderer>().material.color = Color.Lerp(black, clear, normalizedTime);
					yield return null;
				}
				fadeSphere.gameObject.SetActive(false);
			}
			else
			{
				active.gameObject.SetActive(false);
				Debug.Log("All Games Completed");
			}
		}

		[ContextMenu("test")]
		public void FadeOut()
		{
			StartCoroutine(_FadeOut(fadeTime));
		}

		IEnumerator _FadeOut(float t)
		{
			var start = Time.time;
			var elapsed = 0f;
			var normalizedTime = 0f;
			fadeSphere.gameObject.SetActive(true);
			while (elapsed < t)
			{
				elapsed = Time.time - start;
				normalizedTime = Mathf.Clamp(elapsed / t, 0f, 1f);
				fadeSphere.GetComponent<Renderer>().material.color = Color.Lerp(clear, black, normalizedTime);
				yield return null;
			}
		}

		void ActiveGameRoundEnd()
		{
			active.ResetPoints();
			active.RoundDone = true;
			active.PlayRound();
			//do round sound
			roundDoneSound.Play();
		}

		[ContextMenu("Force Next")]
		void DEBUG001()
		{
			StartCoroutine(_GameEnded(fadeTime));
		}

	}
}
