﻿using UnityEngine;
using System;

namespace GTools.Object.Properties
{
	[Serializable]
	public class HSBThreeStageObject : HSBCBaseObj
	{
		public event System.Action<short> OnValuesUpdated;

		[SerializeField]
		private Material m_material;
		private Color m_color = Color.green;
		private float m_blend = 0.0f;
		private float m_hue = 0.0f;
		private float m_saturation = 1.0f;
		private float m_brightness = 0.5f;
		private float m_contrast = 0.5f;


		public Material Material
		{
			get
			{
				return m_material;
			}

			set
			{
				m_material = value;
			}
		}

		public Color Color
		{
			get
			{
				return m_color;
			}

			set
			{
				m_color = value;
			}
		}

		public float Blend
		{
			get
			{
				return m_blend;
			}

			set
			{
				m_blend = value;
			}
		}

		public float Saturation
		{
			get
			{
				return m_saturation;
			}

			set
			{
				value += Offset;
				value = Mathf.Clamp(value, 0.0f, 3.0f);
				m_saturation = value;
			}
		}

		public float Brightness
		{
			get
			{
				return m_brightness;
			}

			set
			{
				m_brightness = value;
			}
		}

		public float Contrast
		{
			get
			{
				return m_contrast;
			}

			set
			{
				m_contrast = value;
			}
		}

		public float Hue
		{
			get
			{
				return m_hue;
			}

			set
			{
				m_hue = value;
			}
		}

		public void OnEnable()
		{
			Init();
		}

		public void Init()
		{

			//Material = GetComponent<Renderer>().sharedMaterial;
		
			Color = Material.GetColor("_Color");
			Hue = Material.GetFloat("_Hue");
			Saturation = Material.GetFloat("_Saturation");
			Brightness = Material.GetFloat("_Brightness");
			Contrast = Material.GetFloat("_Contrast");
			Material.SetFloat("_Cutoff", 0.0f);
		}


		public override void UpdateValues()
		{
			Material.SetFloat("_Hue", Hue);
			Material.SetColor("_Color", Color);
			Material.SetFloat("_Blend", Blend);
			Material.SetFloat("_Saturation", Mathf.Clamp(Saturation + Offset, 0f, 3f));
			Material.SetFloat("_Brightness", Brightness);
			Material.SetFloat("_Contrast", Contrast);
		}

		public void InvokeOnValuesUpdated(short clipUUID)
		{
			if( OnValuesUpdated != null )
			{
				OnValuesUpdated(clipUUID);
			}
		}

		[ContextMenu("Reset Bush")]
		void DEBUG00()
		{
			Init();
			Hue = 0.0f;
			Saturation = 1.0f;
			Brightness = 0.5f;
			Contrast = 0.5f;
			Blend = 0.0f;
			Color = Color.white;
			UpdateValues();
		}
	}
}

