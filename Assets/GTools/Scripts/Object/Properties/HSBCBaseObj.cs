﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HSBCBaseObj : MonoBehaviour {

	private float m_offset;

	public float Offset
	{
		get
		{
			return m_offset;
		}

		set
		{
			m_offset = value;
		}
	}

	public virtual void UpdateValues()
	{ }
}
