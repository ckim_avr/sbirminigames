﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GTools.Water;
namespace GTools.Object.Properties
{
	[RequireComponent(typeof(Displace))]
	public class WaterController : MonoBehaviour
	{

		private Transform m_objectTransform;
		private Material m_material;
		private Vector4 m_distortion;
		private Vector4 m_autoBlendParams;
		private Vector4 m_animationTiling;
		private Vector4 m_animationDirection;
		private Vector4 m_bumpTiling;
		private Vector4 m_bumpDirectionAndSpeed;
		private float m_fresnelScale;
		private Color m_baseColor;
		private Color m_reflectionColor;
		private Color m_specularColor;
		private Vector4 m_specularLightDirection;
		private float m_shininess;
		private float m_perVertexDisplacement;
		private Vector4 m_waveAmplitude;
		private Vector4 m_waveFrequency;
		private Vector4 m_waveSteepness;
		private Vector4 m_waveSpeed;
		private Vector4 m_waveDirectionAB;
		private Vector4 m_waveDirectionCD;

		public Transform ObjectTransform
		{
			get
			{
				return m_objectTransform;
			}

			set
			{
				m_objectTransform = value;
			}
		}

		public Material Material
		{
			get
			{
				return m_material;
			}

			set
			{
				m_material = value;
			}
		}

		public Vector4 Distortion
		{
			get
			{
				return m_distortion;
			}

			set
			{
				m_distortion = value;
			}
		}

		public Vector4 AutoBlendParams
		{
			get
			{
				return m_autoBlendParams;
			}

			set
			{
				m_autoBlendParams = value;
			}
		}

		public Vector4 AnimationTiling
		{
			get
			{
				return m_animationTiling;
			}

			set
			{
				m_animationTiling = value;
			}
		}

		public Vector4 AnimationDirection
		{
			get
			{
				return m_animationDirection;
			}

			set
			{
				m_animationDirection = value;
			}
		}

		public Vector4 BumpTiling
		{
			get
			{
				return m_bumpTiling;
			}

			set
			{
				m_bumpTiling = value;
			}
		}

		public Vector4 BumpDirectionAndSpeed
		{
			get
			{
				return m_bumpDirectionAndSpeed;
			}

			set
			{
				m_bumpDirectionAndSpeed = value;
			}
		}

		public float FresnelScale
		{
			get
			{
				return m_fresnelScale;
			}

			set
			{
				m_fresnelScale = value;
			}
		}

		public Color BaseColor
		{
			get
			{
				return m_baseColor;
			}

			set
			{
				m_baseColor = value;
			}
		}

		public Color ReflectionColor
		{
			get
			{
				return m_reflectionColor;
			}

			set
			{
				m_reflectionColor = value;
			}
		}

		public Color SpecularColor
		{
			get
			{
				return m_specularColor;
			}

			set
			{
				m_specularColor = value;
			}
		}

		public Vector4 SpecularLightDirection
		{
			get
			{
				return m_specularLightDirection;
			}

			set
			{
				m_specularLightDirection = value;
			}
		}

		public float Shininess
		{
			get
			{
				return m_shininess;
			}

			set
			{
				m_shininess = value;
			}
		}

		public float PerVertexDisplacement
		{
			get
			{
				return m_perVertexDisplacement;
			}

			set
			{
				m_perVertexDisplacement = value;
			}
		}

		public Vector4 WaveAmplitude
		{
			get
			{
				return m_waveAmplitude;
			}

			set
			{
				m_waveAmplitude = value;
			}
		}

		public Vector4 WaveFrequency
		{
			get
			{
				return m_waveFrequency;
			}

			set
			{
				m_waveFrequency = value;
			}
		}

		public Vector4 WaveSteepness
		{
			get
			{
				return m_waveSteepness;
			}

			set
			{
				m_waveSteepness = value;
			}
		}

		public Vector4 WaveSpeed
		{
			get
			{
				return m_waveSpeed;
			}

			set
			{
				m_waveSpeed = value;
			}
		}

		public Vector4 WaveDirectionAB
		{
			get
			{
				return m_waveDirectionAB;
			}

			set
			{
				m_waveDirectionAB = value;
			}
		}

		public Vector4 WaveDirectionCD
		{
			get
			{
				return m_waveDirectionCD;
			}

			set
			{
				m_waveDirectionCD = value;
			}
		}

		public void Init()
		{
			//Object Component assignment
			ObjectTransform = GetComponent<Transform>();
			Material = GetComponent<Renderer>().sharedMaterial;

			//Shader Component assignment
			Distortion = Material.GetVector("_DistortParams");
			AutoBlendParams = Material.GetVector("_InvFadeParameter");
			AnimationTiling = Material.GetVector("_AnimationTiling");
			AnimationDirection = Material.GetVector("_AnimationDirection");
			BumpTiling = Material.GetVector("_BumpTiling");
			BumpDirectionAndSpeed = Material.GetVector("_BumpDirection");
			FresnelScale = Material.GetFloat("_FresnelScale");
			BaseColor = Material.GetColor("_BaseColor");
			ReflectionColor = Material.GetColor("_ReflectionColor");
			SpecularColor = Material.GetColor("_SpecularColor");
			SpecularLightDirection = Material.GetVector("_WorldLightDir");
			Shininess = Material.GetFloat("_Shininess");
			PerVertexDisplacement = Material.GetFloat("_GerstnerIntensity");
			WaveAmplitude = Material.GetVector("_GAmplitude");
			WaveFrequency = Material.GetVector("_GFrequency");
			WaveSteepness = Material.GetVector("_GSteepness");
			WaveSpeed = Material.GetVector("_GSpeed");
			WaveDirectionAB = Material.GetVector("_GDirectionAB");
			WaveDirectionCD = Material.GetVector("_GDirectionCD");
		}



		public void UpdateValues()
		{
			Material.SetVector("_DistortParams", Distortion);
			Material.SetVector("_InvFadeParameter", AutoBlendParams);
			Material.SetVector("_AnimationTiling", AnimationTiling);
			Material.SetVector("_AnimationDirection", AnimationDirection);
			Material.SetVector("_BumpTiling", BumpTiling);
			Material.SetVector("_BumpDirection", BumpDirectionAndSpeed);
			Material.SetFloat("_FresnelScale", FresnelScale);
			Material.SetColor("_BaseColor", BaseColor);
			Material.SetColor("_ReflectionColor", ReflectionColor);
			Material.SetColor("_SpecularColor", SpecularColor);
			Material.SetVector("_WorldLightDir", SpecularLightDirection);
			Material.SetFloat("_Shininess", Shininess);
			Material.SetFloat("_GerstnerIntensity", PerVertexDisplacement);
			Material.SetVector("_GAmplitude", WaveAmplitude);
			Material.SetVector("_GFrequency", WaveFrequency);
			Material.SetVector("_GSteepness", WaveSteepness);
			Material.SetVector("_GSpeed", WaveSpeed);
			Material.SetVector("_GDirectionAB", WaveDirectionAB);
			Material.SetVector("_GDirectionCD", WaveDirectionCD);
		}
	}
}