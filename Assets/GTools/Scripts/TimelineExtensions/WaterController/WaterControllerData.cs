﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace GTools.TimelineExtensions.Playables
{
	[Serializable]
	public class WaterControllerDataPlayable : PlayableBehaviour //IOSCTimelineClip //removed IOSCTimelineClip interface until we need it
	{

		[Range(-5.0f, 5.0f)]
		public float height = 0.0f;
		public Vector4 distortion = new Vector4(1.028571f, 0.2018722f, 2.766625f, -0.4171428f);
		public Vector4 autoBlendParams = new Vector4(0.15f, 0.15f, 0.5f, 1.0f);
		public Vector4 animationTiling = new Vector4(0f,0f,0f,0f);
		public Vector4 animationDirection = new Vector4(1f,1f,1f,1f);
		public Vector4 bumpTiling = new Vector4(0.04f, 0.04f, 0.04f, 0.08f);
		public Vector4 bumpDirectionAndSpeed = new Vector4(1f,1f,1f,0f);
		public float fresnelScale = 0.6f;
		public Color baseColor = new Color(0.094f, 0.13f, 0.184f, 0.059f);
		public Color reflectionColor = new Color(0.66f, 0.28f, 0.03f, 0.22f);
		public Color specularColor = new Color(0.96f, 0.66f, 0.28f, 0.31f);
		public Vector4 specularLightDirection = new Vector4(0f, -0.0533817f, 0.9985742f, 0f);
		public float shininess = 290f;
		public float perVertexDisplacement = 1.0f;
		public Vector4 waveAmplitude = new Vector4(0.13f, 0.05f, 0.29f, 0.02f);
		public Vector4 waveFrequency = new Vector4(0.5f, 4.65f, 0.6f, 0.245f);
		public Vector4 waveSteepness = new Vector4(1f,1f,1f,1f);
		public Vector4 waveSpeed = new Vector4(0f,0f,1f,1f);
		public Vector4 waveDirectionAB = new Vector4(0.85f, 0.3f, 0.25f, 0.25f);
		public Vector4 waveDirectionCD = new Vector4(-0.3f, -0.9f, 0.5f, 0.5f);

		public short ClipID
		{
			get;
			set;
		}
	}

	public class WaterControllerData : PlayableAsset, ITimelineClipAsset
	{
		public WaterControllerDataPlayable waterData = new WaterControllerDataPlayable();
		[System.NonSerialized]
		public GameObject owner;
		[HideInInspector]
		public short clipID = 0;
	
		public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
		{
			this.owner = owner;
			GenerateClipID();

			waterData.ClipID = clipID;

			return ScriptPlayable<WaterControllerDataPlayable>.Create(graph, waterData);
			
		}

		public ClipCaps clipCaps
		{
			get { return ClipCaps.Blending | ClipCaps.Extrapolation;  }
		}

		private void GenerateClipID()
		{
			// If we are missing a clipID, assign a new one
			if (clipID == 0)
			{
				clipID = (short)UnityEngine.Random.Range(short.MinValue, short.MaxValue);
			}

			// Check if current ID is currently in use
			SimilarIDCheck();
		}

		private void SimilarIDCheck()
		{
			// Get all existing HSBThreeStageData objects
			WaterControllerData[] hsb3clips = Resources.FindObjectsOfTypeAll<WaterControllerData>();
			bool foundSameClipID = false;
			// Check for addresses meanwhile we keep finding similar IDs
			do
			{
				foundSameClipID = false;
				for (int i = 0; i < hsb3clips.Length; i++)
				{
					if (hsb3clips[i] == this)
						continue;

					var otherClip = hsb3clips[i];
					if (otherClip.clipID.Equals(this.clipID))
					{
						foundSameClipID = true;
						break;
					}
				}

				// If we found a smiliar ID, assign new ID and search again
				if (foundSameClipID)
				{
					clipID = (short)UnityEngine.Random.Range(short.MinValue, short.MaxValue);
				}
			} while (foundSameClipID == true);
		}
	}


}
