﻿using GTools.Object.Properties;
using GTools.TimelineExtensions.Playables;
using UnityEngine;
using UnityEngine.Playables;

namespace GTools.TimelineExtensions.Playables
{

	public class WaterControllerMixer : PlayableBehaviour
	{
		float m_defaultHeight;
		Vector4 m_defaultDistortion;
		Vector4 m_defaultAutoBlendParams;
		Vector4 m_defaultAnimationTiling;
		Vector4 m_defaultAnimationDirection;
		Vector4 m_defaultBumpTiling;
		Vector4 m_defaultBumpDirectionAndSpeed;
		float m_defaultFresnelScale;
		Color m_defaultBaseColor;
		Color m_defaultReflectionColor;
		Color m_defaultSpecularColor;
		Vector4 m_defaultSpecularLightDirection;
		float m_defaultShininess;
		float m_defaultPerVertexDisplacement;
		Vector4 m_defaultWaveAmplitude;
		Vector4 m_defaultWaveFrequency;
		Vector4 m_defaultWaveSteepness;
		Vector4 m_defaultWaveSpeed;
		Vector4 m_defaultWaveDirectionAB;
		Vector4 m_defaultWaveDirectionCD;

		bool m_FirstFrameHappened = false;

		public override void ProcessFrame(Playable handle, FrameData info, object playerData)
		{
			var waterObject = playerData as WaterController;
			if (waterObject == null)
				return;
			if (!m_FirstFrameHappened)
			{
				waterObject.Init();

				m_defaultHeight = waterObject.ObjectTransform.position.y;
				m_defaultDistortion = waterObject.Distortion;
				m_defaultAutoBlendParams = waterObject.AutoBlendParams;
				m_defaultAnimationTiling = waterObject.AnimationTiling;
				m_defaultAnimationDirection = waterObject.AnimationDirection;
				m_defaultBumpTiling = waterObject.BumpTiling;
				m_defaultBumpDirectionAndSpeed = waterObject.BumpDirectionAndSpeed;
				m_defaultFresnelScale = waterObject.FresnelScale;
				m_defaultBaseColor = waterObject.BaseColor;
				m_defaultReflectionColor = waterObject.ReflectionColor;
				m_defaultSpecularColor = waterObject.SpecularColor;
				m_defaultSpecularLightDirection = waterObject.SpecularLightDirection;
				m_defaultShininess = waterObject.Shininess;
				m_defaultPerVertexDisplacement = waterObject.PerVertexDisplacement;
				m_defaultWaveAmplitude = waterObject.WaveAmplitude;
				m_defaultWaveFrequency = waterObject.WaveFrequency;
				m_defaultWaveSteepness = waterObject.WaveSteepness;
				m_defaultWaveSpeed = waterObject.WaveSpeed;
				m_defaultWaveDirectionAB = waterObject.WaveDirectionAB;
				m_defaultWaveDirectionCD = waterObject.WaveDirectionCD;

				m_FirstFrameHappened = true;
			}



			float height = m_defaultHeight;
			Vector4 distortion = m_defaultDistortion;
			Vector4 autoBlendParams = m_defaultAutoBlendParams;
			Vector4 animationTiling = m_defaultAnimationTiling;
			Vector4 animationDirection = m_defaultAnimationDirection;
			Vector4 bumpTiling = m_defaultBumpTiling;
			Vector4 bumpDirectionAndSpeed = m_defaultBumpDirectionAndSpeed;
			float fresnelScale = m_defaultFresnelScale;
			Color baseColor = m_defaultBaseColor;
			Color reflectionColor = m_defaultReflectionColor;
			Color specularColor = m_defaultSpecularColor;
			Vector4 specularLightDirection = m_defaultSpecularLightDirection;
			float shininess = m_defaultShininess;
			float perVertexDisplacement = m_defaultPerVertexDisplacement;
			Vector4 waveAmplitude = m_defaultWaveAmplitude;
			Vector4 waveFrequency = m_defaultWaveFrequency;
			Vector4 waveSteepness = m_defaultWaveSteepness;
			Vector4 waveSpeed = m_defaultWaveSpeed;
			Vector4 waveDirectionAB = m_defaultWaveDirectionAB;
			Vector4 waveDirectionCD = m_defaultWaveDirectionCD;

			var count = handle.GetInputCount();
			for (int i = 0; i < count; i++)
			{
				var inputHandle = handle.GetInput(i);
				var weight = handle.GetInputWeight(i);

				if (inputHandle.IsValid() && inputHandle.GetPlayState() == PlayState.Playing && weight > 0)
				{
					var data = ((ScriptPlayable<WaterControllerDataPlayable>)inputHandle).GetBehaviour();
					if (data != null)
					{
						height = Mathf.Lerp(height, data.height, weight);
						distortion = Vector4.Lerp(distortion, data.distortion, weight);
						autoBlendParams = Vector4.Lerp(autoBlendParams, data.autoBlendParams, weight);
						animationTiling = Vector4.Lerp(animationTiling, data.animationTiling, weight);
						animationDirection = Vector4.Lerp(animationDirection, data.animationDirection, weight);
						bumpTiling = Vector4.Lerp(bumpTiling, data.bumpTiling, weight);
						bumpDirectionAndSpeed = Vector4.Lerp(bumpDirectionAndSpeed, data.bumpDirectionAndSpeed, weight);
						fresnelScale = Mathf.Lerp(fresnelScale, data.fresnelScale, weight);
						baseColor = Color.Lerp(baseColor, data.baseColor, weight);
						reflectionColor = Color.Lerp(reflectionColor, data.reflectionColor, weight);
						specularColor = Color.Lerp(specularColor, data.specularColor, weight);
						specularLightDirection = Vector4.Lerp(specularLightDirection, data.specularLightDirection, weight);
						shininess = Mathf.Lerp(shininess, data.shininess, weight);
						perVertexDisplacement = Mathf.Lerp(perVertexDisplacement, data.perVertexDisplacement, weight);
						waveAmplitude = Vector4.Lerp(waveAmplitude, data.waveAmplitude, weight);
						waveFrequency = Vector4.Lerp(waveFrequency, data.waveFrequency, weight);
						waveSteepness = Vector4.Lerp(waveSteepness, data.waveSteepness, weight);
						waveSpeed = Vector4.Lerp(waveSpeed, data.waveSpeed, weight);
						waveDirectionAB = Vector4.Lerp(waveDirectionAB, data.waveDirectionAB, weight);
						waveDirectionCD = Vector4.Lerp(waveDirectionCD, data.waveDirectionCD, weight);
					}
				}
			}

			waterObject.ObjectTransform.position = new Vector3(waterObject.ObjectTransform.position.x, height, waterObject.ObjectTransform.position.z);
			waterObject.Distortion = distortion;
			waterObject.AutoBlendParams = autoBlendParams;
			waterObject.AnimationTiling = animationTiling;
			waterObject.AnimationDirection = animationDirection;
			waterObject.BumpTiling = bumpTiling;
			waterObject.BumpDirectionAndSpeed = bumpDirectionAndSpeed;
			waterObject.FresnelScale = fresnelScale;
			waterObject.BaseColor = baseColor;
			waterObject.ReflectionColor = reflectionColor;
			waterObject.SpecularColor = specularColor;
			waterObject.SpecularLightDirection = specularLightDirection;
			waterObject.Shininess = shininess;
			waterObject.PerVertexDisplacement = perVertexDisplacement;
			waterObject.WaveAmplitude = waveAmplitude;
			waterObject.WaveFrequency = waveFrequency;
			waterObject.WaveSteepness = waveSteepness;
			waterObject.WaveSpeed = waveSpeed;
			waterObject.WaveDirectionAB = waveDirectionAB;
			waterObject.WaveDirectionCD = waveDirectionCD;
			waterObject.UpdateValues();


		}
	}
}