﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using GTools.Object.Properties;
using GTools.TimelineExtensions.Playables;


namespace GTools
{
	[TrackColor(0.0f, 0.3f, 0.9f)]
	[TrackClipType(typeof(WaterControllerData))]
	[TrackBindingType(typeof(WaterController))]
	public class WaterControllerTrack : TrackAsset
	{
		public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			return ScriptPlayable<WaterControllerMixer>.Create(graph, inputCount);
		}
	}
}
