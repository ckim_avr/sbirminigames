﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace GTools.TimelineExtensions.Playables
{
	[Serializable]
	public class SkyboxThreeStageDataPlayable : PlayableBehaviour
	{

		public bool skyGradient = true;
		public Color topColor = new Color(0.1875f, 0.1875f, 0.4140625f, 1.0f );
		public Color horizonColor = new Color(0.2265f, 0.2578f, 0.3349f, 1.0f);
		public Color bottomColor;
		[Range(0.0f, 4.0f)]
		public float topSkyColorBlending = 1.0f;
		[Range(0.0f, 4.0f)]
		public float bottomSkyColorBlending = 1.0f;

		public bool stars;
		public Color starColor;


		public bool horizonHalo = true;
		public Color horizonHaloColor;
		public float horizonHaloIntensity = 1.0f;
		public float horizonHaloSize = 1.0f;

		public bool clouds = true;
		public Color cloudColor;
		public Color cloudShadow;
		[Range(0.0f,2.0f)]
		public float cloudBending = 0.6f;
		[Range(-0.5f, 1.0f)]
		public float cloudFilter = 0.6f;
		public float lowAlt = 10.0f;
		public float highAlt = 20.0f;
		public Vector4 cloudOffset;
		public Vector4 cloudParallax;
		public Vector4 cloudSpeed;


		public bool environmentMap = true;
		[Range(-1.0f, 1.0f)]
		public float environmentBlend = 0.0f;
		public Color environmentColor = Color.white;
		[Range(-1.0f, 1.0f)]
		public float hue = 0.0f;
		[Range(0.0f, 3.0f)]
		public float saturation = 0.5f;
		[Range(0.5f, 1.0f)]
		public float brightness = 0.5f;
		[Range(0.5f, 1.0f)]
		public float contrast = 0.5f;

	}

	public class SkyboxThreeStageData : PlayableAsset, ITimelineClipAsset
	{
		public SkyboxThreeStageDataPlayable skyboxData = new SkyboxThreeStageDataPlayable();
		[System.NonSerialized]
		public GameObject owner;

		public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
		{
			this.owner = owner;
			return ScriptPlayable<SkyboxThreeStageDataPlayable>.Create(graph, skyboxData);
		}

		public ClipCaps clipCaps
		{
			get { return ClipCaps.Blending | ClipCaps.Extrapolation; }
		}
	}
}

