﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using GTools.TimelineExtensions.Playables;
using GTools.Object.Properties;

namespace GTools
{
	[TrackColor(0.0f, 0.3f, 0.9f)]
	[TrackClipType(typeof(SkyboxThreeStageData))]
	[TrackBindingType(typeof(SkyboxThreeStageObject))]
	public class SkyboxThreeStageTrack : TrackAsset {
		public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			return ScriptPlayable<SkyboxThreeStageMixer>.Create(graph, inputCount);
		}
	}
}

