﻿using GTools.Object.Properties;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
//using GTools.Object.Properties;


namespace GTools.TimelineExtensions.Playables
{
	public class SkyboxThreeStageMixer : PlayableBehaviour {
	
		//default sky gradient
		bool m_DefaultSkyGradient;
		Color m_DefaultTopColor;
		Color m_DefaultHorizonColor;
		Color m_DefaultBottomColor;
		float m_DefaultTopSkyColorBlending;
		float m_DefaultBottomSkyColorBlending;
	
	
		//default stars
		bool m_DefaultStars;
		Color m_DefaultStarColor;
	
		//default horizon halo
		bool m_DefaultHorizonHalo;
		Color m_DefaultHorizonHaloColor;
		float m_DefaultHorizonHaloIntensity;
		float m_DefaultHorizonHaloSize;
	
		//default clouds
		bool m_DefaultClouds;
		Color m_DefaultCloudColor;
		Color m_DefaultCloudShadow;
		float m_DefaultCloudBending;
		float m_DefaultCloudFilter;
		float m_DefaultLowAlt;
		float m_DefaultHighAlt;
		Vector4 m_DefaultCloudOffset;
		Vector4 m_DefaultCloudParallax;
		Vector4 m_DefaultCloudSpeed;
	
		//default environment
		bool m_DefaultEnvironmentMap;
		float m_DefaultEnvironmentBlend;
		Color m_DefaultEnvColor;
		float m_DefaultHue;
		float m_DefaultSaturation;
		float m_DefaultBrightness;
		float m_DefaultContrast;
	
		//bool for ??? and glory
		bool m_FirstFrameHappened = false;
	
		public override void ProcessFrame(Playable handle, FrameData info, object playerData)
		{
			var skyboxThreeStageObj = playerData as SkyboxThreeStageObject;
			if (skyboxThreeStageObj == null)
				return;
	
			if (!m_FirstFrameHappened)
			{
				skyboxThreeStageObj.Init();
				m_FirstFrameHappened = true;
			}
			m_DefaultSkyGradient = skyboxThreeStageObj.SkyGradient;
			m_DefaultTopColor = skyboxThreeStageObj.TopColor;
			m_DefaultHorizonColor = skyboxThreeStageObj.HorizonColor;
			m_DefaultBottomColor  = skyboxThreeStageObj.BottomColor;
			m_DefaultTopSkyColorBlending = skyboxThreeStageObj.TopSkyColorBlending;
			m_DefaultBottomSkyColorBlending  = skyboxThreeStageObj.BottomSkyColorBlending;
			
			m_DefaultStars  = skyboxThreeStageObj.Stars;
			m_DefaultStarColor  = skyboxThreeStageObj.StarColor;
			
			m_DefaultHorizonHalo = skyboxThreeStageObj.HorizonHalo;
			m_DefaultHorizonHaloColor = skyboxThreeStageObj.HorizonHaloColor;
			m_DefaultHorizonHaloIntensity = skyboxThreeStageObj.HorizonHaloIntensity;
			m_DefaultHorizonHaloSize = skyboxThreeStageObj.HorizonHaloSize;
			
			m_DefaultClouds  = skyboxThreeStageObj.Clouds;
			m_DefaultCloudColor = skyboxThreeStageObj.CloudColor;
			m_DefaultCloudShadow = skyboxThreeStageObj.CloudShadow;
			m_DefaultCloudBending = skyboxThreeStageObj.CloudBending;
			m_DefaultCloudFilter  = skyboxThreeStageObj.CloudFilter;
			m_DefaultLowAlt = skyboxThreeStageObj.LowAlt;
			m_DefaultHighAlt = skyboxThreeStageObj.HighAlt;
			m_DefaultCloudOffset = skyboxThreeStageObj.CloudOffset;
			m_DefaultCloudParallax = skyboxThreeStageObj.CloudParallax;
			m_DefaultCloudSpeed = skyboxThreeStageObj.CloudSpeed;
			
			m_DefaultEnvironmentMap	  = skyboxThreeStageObj.EnvironmentMap;
			m_DefaultEnvironmentBlend = skyboxThreeStageObj.EnvironmentBlend;
			m_DefaultEnvColor = skyboxThreeStageObj.EnvColor;
			m_DefaultHue = skyboxThreeStageObj.Hue;
			m_DefaultSaturation = skyboxThreeStageObj.Saturation;
			m_DefaultBrightness = skyboxThreeStageObj.Brightness;
			m_DefaultContrast = skyboxThreeStageObj.Contrast;

			#if OSC_REALTIME
			// Check if an osc receiver is attached to skybox object
			var receiver = skyboxThreeStageObj.GetComponent<SkyboxThreeStageOSCReceiver>();
			if( receiver != null && receiver.ReceivedNewData )
			{
				// Look for clip/playable with the most weight
				int inputCount = handle.GetInputCount();
				int? heaviestClipIndex = null;
				float maxWeight = 0f;
				for( int i = 0; i < inputCount; i++ )
				{
					Playable inputHandle = handle.GetInput(i);
					float weight = handle.GetInputWeight(i);
					if( !inputHandle.IsValid() || 
						inputHandle.GetPlayState() != PlayState.Playing || 
						weight <= 0f )
						continue;

					if( heaviestClipIndex == null || maxWeight < weight )
					{
						maxWeight = weight;
						heaviestClipIndex = i;
					}
				}
				// Set the data for that playable to the data we received via OSC
				if( heaviestClipIndex != null )
				{
					var heaviestClip = ((ScriptPlayable<SkyboxThreeStageDataPlayable>)(handle.GetInput(heaviestClipIndex.Value))).GetBehaviour();
					if( heaviestClip != null )
					{
						SkyboxThreeStageDataPlayable newData = receiver.GetReceivedData();
						heaviestClip.skyGradient = newData.skyGradient;
						heaviestClip.stars = newData.stars;
						heaviestClip.horizonHalo = newData.horizonHalo;
						heaviestClip.clouds = newData.clouds;
						heaviestClip.environmentMap = newData.environmentMap;

						heaviestClip.topColor = newData.topColor;
						heaviestClip.horizonColor = newData.horizonColor;
						heaviestClip.bottomColor = newData.bottomColor;
						heaviestClip.topSkyColorBlending = newData.topSkyColorBlending;
						heaviestClip.bottomSkyColorBlending = newData.bottomSkyColorBlending;
						heaviestClip.starColor = newData.starColor;
						heaviestClip.horizonHaloColor = newData.horizonHaloColor;
						heaviestClip.horizonHaloIntensity = newData.horizonHaloIntensity;
						heaviestClip.horizonHaloSize = newData.horizonHaloSize;
						heaviestClip.cloudColor = newData.cloudColor;
						heaviestClip.cloudShadow = newData.cloudShadow;
						heaviestClip.cloudBending = newData.cloudBending;
						heaviestClip.cloudFilter = newData.cloudFilter;
						heaviestClip.lowAlt = newData.lowAlt;
						heaviestClip.highAlt = newData.highAlt;
						heaviestClip.cloudOffset = newData.cloudOffset;
						heaviestClip.cloudParallax = newData.cloudParallax;
						heaviestClip.cloudSpeed = newData.cloudSpeed;
						heaviestClip.environmentBlend = newData.environmentBlend;

						heaviestClip.environmentColor = newData.environmentColor;
						heaviestClip.hue = newData.hue;
						heaviestClip.saturation = newData.saturation;
						heaviestClip.brightness = newData.brightness;
						heaviestClip.contrast = newData.contrast;
					}
				}
			}
			#endif

			bool SkyGradient = m_DefaultSkyGradient;
			Color TopColor = m_DefaultTopColor;
			Color HorizonColor = m_DefaultHorizonColor;
			Color BottomColor = m_DefaultBottomColor;
			float TopSkyColorBlending = m_DefaultTopSkyColorBlending;
			float BottomSkyColorBlending = m_DefaultBottomSkyColorBlending;
			bool Stars = m_DefaultStars;
			Color StarColor = m_DefaultStarColor;
			bool HorizonHalo = m_DefaultHorizonHalo;
			Color HorizonHaloColor = m_DefaultHorizonHaloColor;
			float HorizonHaloIntensity = m_DefaultHorizonHaloIntensity;
			float HorizonHaloSize = m_DefaultHorizonHaloSize;
			bool Clouds = m_DefaultClouds;
			Color CloudColor = m_DefaultCloudColor;
			Color CloudShadow = m_DefaultCloudShadow;
			float CloudBending = m_DefaultCloudBending;
			float CloudFilter = m_DefaultCloudFilter;
			float LowAlt = m_DefaultLowAlt;
			float HighAlt = m_DefaultHighAlt;
			Vector4 CloudOffset = m_DefaultCloudOffset;
			Vector4 CloudParallax= m_DefaultCloudParallax;
			Vector4 CloudSpeed = m_DefaultCloudSpeed;
			bool EnvironmentMap = m_DefaultEnvironmentMap;
			float EnvironmentBlend = m_DefaultEnvironmentBlend;
			Color EnvColor = m_DefaultEnvColor;
			float Hue = m_DefaultHue;
			float Saturation = m_DefaultSaturation;
			float Brightness = m_DefaultBrightness;
			float Contrast = m_DefaultContrast;
	
			var count = handle.GetInputCount();
			for (var i = 0; i < count; i++)
			{
				var inputHandle = handle.GetInput(i);
				var weight = handle.GetInputWeight(i);
	
				if (inputHandle.IsValid() &&
					inputHandle.GetPlayState() == PlayState.Playing && weight > 0)
				{
					var data = ((ScriptPlayable<SkyboxThreeStageDataPlayable>)inputHandle).GetBehaviour();
					if (data != null)
					{
	
						if (weight > 0.5f)
						{
							SkyGradient = data.skyGradient;
							Stars = data.stars;
							HorizonHalo = data.horizonHalo;
							Clouds = data.clouds;
							EnvironmentMap = data.environmentMap;
						}
	
						TopColor = Color.Lerp(TopColor, data.topColor, weight);
						HorizonColor = Color.Lerp(HorizonColor, data.horizonColor, weight);
						BottomColor = Color.Lerp(BottomColor, data.bottomColor, weight);
						TopSkyColorBlending = Mathf.Lerp(TopSkyColorBlending, data.topSkyColorBlending, weight);
						BottomSkyColorBlending = Mathf.Lerp(BottomSkyColorBlending, data.bottomSkyColorBlending, weight);
						StarColor = Color.Lerp(StarColor, data.starColor, weight);
						HorizonHaloColor = Color.Lerp(HorizonHaloColor, data.horizonHaloColor, weight);
						HorizonHaloIntensity = Mathf.Lerp(HorizonHaloIntensity, data.horizonHaloIntensity, weight);
						HorizonHaloSize = Mathf.Lerp(HorizonHaloSize, data.horizonHaloSize, weight);
						CloudColor = Color.Lerp(CloudColor, data.cloudColor, weight);
						CloudShadow = Color.Lerp(CloudShadow, data.cloudShadow, weight);
						CloudBending = Mathf.Lerp(CloudBending, data.cloudBending, weight);
						CloudFilter = Mathf.Lerp(CloudFilter, data.cloudFilter, weight);
						LowAlt = Mathf.Lerp(LowAlt, data.lowAlt, weight);
						HighAlt = Mathf.Lerp(HighAlt, data.highAlt, weight);
						CloudOffset = Vector4.Lerp(CloudOffset, data.cloudOffset, weight);
						CloudParallax = Vector4.Lerp(CloudParallax, data.cloudParallax, weight);
						CloudSpeed = Vector4.Lerp(CloudSpeed, data.cloudSpeed, weight);
						EnvironmentBlend = Mathf.Lerp(EnvironmentBlend, data.environmentBlend, weight);
						EnvColor = Color.Lerp(EnvColor, data.environmentColor, weight);
						Hue =  Mathf.Lerp(Hue, data.hue, weight);
						Saturation = Mathf.Lerp(Saturation, data.saturation, weight);
						Brightness = Mathf.Lerp(Brightness, data.brightness, weight);
						Contrast = Mathf.Lerp(Contrast, data.contrast, weight);
					}
				}
			}
			skyboxThreeStageObj.SkyGradient = SkyGradient;
			skyboxThreeStageObj.Stars = Stars;
			skyboxThreeStageObj.HorizonHalo = HorizonHalo;
			skyboxThreeStageObj.Clouds = Clouds;
			skyboxThreeStageObj.EnvironmentMap = EnvironmentMap;
	
			skyboxThreeStageObj.TopColor = TopColor;
			skyboxThreeStageObj.HorizonColor = HorizonColor;
			skyboxThreeStageObj.BottomColor = BottomColor;
			skyboxThreeStageObj.TopSkyColorBlending = TopSkyColorBlending;
			skyboxThreeStageObj.BottomSkyColorBlending = BottomSkyColorBlending;
			skyboxThreeStageObj.StarColor = StarColor;
			skyboxThreeStageObj.HorizonHaloColor = HorizonHaloColor;
			skyboxThreeStageObj.HorizonHaloIntensity = HorizonHaloIntensity;
			skyboxThreeStageObj.HorizonHaloSize = HorizonHaloSize;
			skyboxThreeStageObj.CloudColor = CloudColor;
			skyboxThreeStageObj.CloudShadow = CloudShadow;
			skyboxThreeStageObj.CloudBending = CloudBending;
			skyboxThreeStageObj.CloudFilter = CloudFilter;
			skyboxThreeStageObj.LowAlt = LowAlt;
			skyboxThreeStageObj.HighAlt = HighAlt;
			skyboxThreeStageObj.CloudOffset = CloudOffset;
			skyboxThreeStageObj.CloudParallax = CloudParallax;
			skyboxThreeStageObj.CloudSpeed = CloudSpeed;
			skyboxThreeStageObj.EnvironmentBlend = EnvironmentBlend;
			skyboxThreeStageObj.EnvColor = EnvColor;
			skyboxThreeStageObj.Hue = Hue;
			skyboxThreeStageObj.Saturation = Saturation;
			skyboxThreeStageObj.Brightness = Brightness;
			skyboxThreeStageObj.Contrast = Contrast;
			
			skyboxThreeStageObj.UpdateValues();
		}
	}
}

