﻿using GTools.Object.Properties;
using GTools.TimelineExtensions.Playables;
using UnityEngine;
using UnityEngine.Playables;

namespace GTools.TimelineExtensions.Playables
{
	public class HSBThreeStageMixer : PlayableBehaviour {
	
		Color m_DefaultColor;
		float m_DefaultBlend;
		float m_DefaultHue;
		float m_DefaultSaturation;
		float m_DefaultBrightness;
		float m_DefaultContrast;
	
		bool m_FirstFrameHappened = false;
	
		public override void ProcessFrame(Playable handle, FrameData info, object playerData)
		{
			var hsbObject = playerData as HSBThreeStageObject;
			if (hsbObject == null)
				return;
	
			if (!m_FirstFrameHappened)
			{
				hsbObject.Init();
				m_FirstFrameHappened = true;
			}
				m_DefaultColor = hsbObject.Color;
				m_DefaultBlend = hsbObject.Blend;
				m_DefaultHue = hsbObject.Hue;
				m_DefaultSaturation = hsbObject.Saturation;
				m_DefaultBrightness = hsbObject.Brightness;
				m_DefaultContrast = hsbObject.Contrast;

			#if OSC_REALTIME
			// Check if an osc receiver is attached to hsb object
			var receiver = hsbObject.GetComponent<HSBThreeStageMixerOSCReceiver>();
			if( receiver != null && receiver.ReceivedNewData )
			{
				HSBThreeStageDataPlayable receivedData = receiver.GetReceivedData();
				short clipUUID = receivedData.ClipID;

				// Look for clip/playable with the same UUID
				int inputCount = handle.GetInputCount();
				for( int i = 0; i < inputCount; i++ )
				{
					var inputDataHandle = ((ScriptPlayable<HSBThreeStageDataPlayable>)handle.GetInput(i)).GetBehaviour();
					// Set the data for corresponding playable to the data we received via OSC
					if (inputDataHandle != null && inputDataHandle.ClipID == clipUUID)
					{
						inputDataHandle.color = receivedData.color;
						inputDataHandle.blend = receivedData.blend;
						inputDataHandle.hue = receivedData.hue;
						inputDataHandle.saturation = receivedData.saturation;
						inputDataHandle.brightness = receivedData.brightness;
						inputDataHandle.contrast = receivedData.contrast;
					}
				}
			}
			#endif
	
			Color col = m_DefaultColor;
			float blend = m_DefaultBlend;
			float hue = m_DefaultHue;
			float saturation = m_DefaultSaturation;
			float brightness = m_DefaultBrightness;
			float contrast = m_DefaultContrast;
			var count = handle.GetInputCount();
			for (var i = 0; i < count; i++)
			{
				var inputHandle = handle.GetInput(i);
				var weight = handle.GetInputWeight(i);
	
				if (inputHandle.IsValid() &&
					inputHandle.GetPlayState() == PlayState.Playing &&
					weight > 0)
				{
					var data = ((ScriptPlayable<HSBThreeStageDataPlayable>)inputHandle).GetBehaviour();
					if (data != null)
					{
						//col.r = Mathf.Lerp(col.r, data.color.r, weight);
						//col.g = Mathf.Lerp(col.g, data.color.g, weight);
						//col.b = Mathf.Lerp(col.b, data.color.b, weight);
	
						col = Color.Lerp(col, data.color, weight);
						blend = (blend == data.blend) ? data.blend : Mathf.Lerp(blend, data.blend, weight);
						hue = (hue == data.hue) ? data.hue : Mathf.Lerp(hue, data.hue, weight);
						saturation =  (saturation == data.saturation) ? data.saturation : Mathf.Lerp(saturation, data.saturation, weight);
						brightness = (brightness == data.brightness) ? data.brightness : Mathf.Lerp(brightness, data.brightness, weight);
						contrast = (contrast == data.contrast) ? data.contrast : Mathf.Lerp(contrast, data.contrast, weight);
	
						//col = Color.Lerp(data.color, bushObject.Color, weight);
					}
				}
			}
			//bushObject.UpdateValues();
			hsbObject.Color = col;
			hsbObject.Blend = blend;
			hsbObject.Hue = hue;
			hsbObject.Saturation = saturation;
			hsbObject.Brightness = brightness;
			hsbObject.Contrast = contrast;
			hsbObject.UpdateValues();
		}
	}
}
