﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace GTools.TimelineExtensions.Playables
{
	[Serializable]
	public class HSBThreeStageDataPlayable : PlayableBehaviour // IOSCTimelineClip //Removed interface until we need it
	{
		public Color color = Color.white;
		[Range(-1.0f, 1.0f)]
		public float blend = 0.0f;
		[Range(-1.0f, 1.0f)]
		public float hue = 0.0f;
		[Range(0.0f, 3.0f)]
		public float saturation = 0.5f;
		[Range(0.5f, 1.0f)]
		public float brightness = 0.5f;
		[Range(0.5f, 1.0f)]
		public float contrast = 0.5f;

		public short ClipID
		{
			get;
			set;
		}
	}

	public class HSBThreeStageData : PlayableAsset, ITimelineClipAsset
	{

		public HSBThreeStageDataPlayable hsbData = new HSBThreeStageDataPlayable();
		[System.NonSerialized]
		public GameObject owner;
		[HideInInspector]
		public short clipID = 0;

		public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
		{
		
			this.owner = owner;
			// Get get unique clipID for clip
			GenerateClipID();
			// Assign unique clipID for to hsbData before it is serialized
			hsbData.ClipID = clipID;

			return ScriptPlayable<HSBThreeStageDataPlayable>.Create(graph, hsbData);
		}

		public ClipCaps clipCaps
		{
			get { return ClipCaps.Blending | ClipCaps.Extrapolation; }
		}

		private void GenerateClipID()
		{
			// If we are missing a clipID, assign a new one
			if (clipID == 0)
			{
				clipID = (short)UnityEngine.Random.Range(short.MinValue, short.MaxValue);
			}

			// Check if current ID is currently in use
			SimilarIDCheck();
		}

		private void SimilarIDCheck()
		{
			// Get all existing HSBThreeStageData objects
			HSBThreeStageData[] hsb3clips = Resources.FindObjectsOfTypeAll<HSBThreeStageData>();
			bool foundSameClipID = false;
			// Check for addresses meanwhile we keep finding similar IDs
			do
			{
				foundSameClipID = false;
				for (int i = 0; i < hsb3clips.Length; i++)
				{
					if (hsb3clips[i] == this)
						continue;

					var otherClip = hsb3clips[i];
					if ( otherClip.clipID.Equals(this.clipID))
					{
						foundSameClipID = true;
						break;
					}
				}

				// If we found a smiliar ID, assign new ID and search again
				if (foundSameClipID)
				{
					clipID = (short)UnityEngine.Random.Range(short.MinValue, short.MaxValue);
				}
			} while (foundSameClipID == true);
		}
	}
}