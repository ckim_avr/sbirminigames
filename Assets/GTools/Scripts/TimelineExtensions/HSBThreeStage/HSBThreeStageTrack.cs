﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using GTools.Object.Properties;
using GTools.TimelineExtensions.Playables;

namespace GTools
{
[TrackColor(0.0f, 0.3f, 0.9f)]
[TrackClipType(typeof(HSBThreeStageData))]
[TrackBindingType(typeof(HSBThreeStageObject))]
public class HSBThreeStageTrack : TrackAsset {
		public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
		{
			return ScriptPlayable<HSBThreeStageMixer>.Create(graph, inputCount);
		}
	}
}
