﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GTools.Object.Properties;
using System;

namespace GTools.Utils.Extensions
{
	public static class ExtensionMethods
	{
		private static System.Random rng = new System.Random();

		public static void Shuffle<T>(this IList<T> list)
		{
			int n = list.Count;
			while (n > 1)
			{
				n--;
				int k = rng.Next(n + 1);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}

		/// <summary>
		/// Extension methods for Animators
		/// </summary>
		/// <param name="self"></param>
		/// <param name="percent"></param>
		/// <param name="layer"></param>
		/// <returns></returns>
		public static Animator SetTimeForCurrentClip(this Animator self, float percent, int layer = 1)
		{
			AnimatorClipInfo[] cInfo = self.GetCurrentAnimatorClipInfo(layer);
			if (cInfo.Length > 0)
			{
				AnimationClip clip = cInfo[0].clip;
				self.Play(clip.name, layer, clip.length * percent);
			}
			return self;
		}

		//Commented out until AmbientSoundController is implemented
		//public static AmbientSoundController.Controls UpdateState(this AmbientSoundController.Controls self,
		//	AudioClip chOne, 
		//	AudioClip chTwo, 
		//	AudioClip chThree, 
		//	AudioClip chFour, 
		//	float chOneVol, 
		//	float chTwoVol, 
		//	float chThreeVol, 
		//	float chFourVol)
		//	
		//{
		//	self.channelOne.channel.Clip = chOne;
		//	self.channelTwo.channel.Clip = chTwo;
		//	self.channelThree.channel.Clip = chThree;
		//	self.channelFour.channel.Clip = chFour;
		//
		//	self.channelOne.channel.Volume = chOneVol;
		//	self.channelTwo.channel.Volume = chTwoVol;
		//	self.channelThree.channel.Volume = chThreeVol;
		//	self.channelFour.channel.Volume = chFourVol;
		//	return self;
		//}

		public static float Remap(this float self, float from1 , float to1, float from2, float to2)
		{
			return (self - from1) / (to1 - from1) * (to2 - from2) + from2;
		}

		public static void MapAllFields(object source, object dst)
		{
			System.Reflection.FieldInfo[] ps = source.GetType().GetFields();
			foreach (var item in ps)
			{
				var o = item.GetValue(source);
				var p = dst.GetType().GetField(item.Name);
				if (p != null)
				{
					Type t = Nullable.GetUnderlyingType(p.FieldType) ?? p.FieldType;
					object safeValue = (o == null) ? null : Convert.ChangeType(o, t);
					p.SetValue(dst, safeValue);
				}
			}
		}
	}
}