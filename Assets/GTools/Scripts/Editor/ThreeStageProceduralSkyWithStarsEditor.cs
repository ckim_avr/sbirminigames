﻿using UnityEngine;
using UnityEditor;

public class ThreeStageProceduralSkyWithStarsEditor : ShaderGUI  {

	private MaterialProperty environmentToggle = null;
	//private MaterialProperty environmentCubemap = null;
	private MaterialProperty lowEnvironmentMap = null;
	private MaterialProperty midEnvironmentMap = null;
	private MaterialProperty highEnvironmentMap = null;
	private MaterialProperty environmentBlend = null;
	private MaterialProperty envColor = null;
	private MaterialProperty hue = null;
	private MaterialProperty saturation = null;
	private MaterialProperty brightness = null;
	private MaterialProperty contrast = null;
	//private MaterialProperty threshold = null;
	//private MaterialProperty slope = null;
	//private MaterialProperty keyColor = null;



	private MaterialProperty cloudToggle = null;
	private MaterialProperty cloudColor = null;
	private MaterialProperty cloudShadow = null;
	private MaterialProperty cloudLowTex = null;
	private MaterialProperty cloudHighTex = null;
	private MaterialProperty cloudWaveTex = null;
	private MaterialProperty cloudBending = null;
	private MaterialProperty cloudFilter = null;
	private MaterialProperty cloudLowAlt = null;
	private MaterialProperty cloudHighAlt = null;
	private MaterialProperty cloudOffset = null;
	private MaterialProperty cloudParallax = null;
	private MaterialProperty cloudSpeed = null;


	private MaterialProperty skyGradientToggle = null;
	private MaterialProperty topColor = null;
	private MaterialProperty horizonColor = null;
	private MaterialProperty bottomColor = null;
	private MaterialProperty topSkyColorBlending = null;
	private MaterialProperty bottomSkyColorBlending = null;

	private MaterialProperty sunToggle = null;
	private MaterialProperty sunColor = null;
	private MaterialProperty sunIntensities = null;
	private MaterialProperty sunSharpness = null;

	private MaterialProperty sunHaloToggle = null;
	private MaterialProperty sunHaloColor = null;
	private MaterialProperty sunHaloIntensities = null;
	private MaterialProperty sunHaloSize = null;

	private MaterialProperty horizonHaloToggle = null;
	private MaterialProperty horizonHaloColor = null;
	private MaterialProperty horizonHaloIntensities = null;
	private MaterialProperty horizonHaloSize = null;

	private MaterialProperty starToggle = null;
	private MaterialProperty starTexture = null;
	private MaterialProperty starColor = null;

	private const string SKY_GRADIENT_KEYWORD = "PS_BACKGROUND_GRADIENT";
	private const string SUN_KEYWORD = "PS_SUN";
	private const string SUN_HALO_KEYWORD = "PS_SUN_HALO";
	private const string HORIZON_HALO_KEYWORD = "PS_HORIZON_HALO";
	private const string CLOUD_KEYWORD = "PS_CLOUDS";
	private const string ENV_KEYWORD = "PS_ENVIRONMENT";
	private const string STAR_KEYBWORD = "PS_STARS";
	private bool displayGradientSettings = true;
	private bool displaySunSettings = true;
	private bool displaySunHaloSettings = true;
	private bool displayHorizonHaloSettings = true;
	private bool displayCloudSettings = true;
	private bool displayEnvironmentSettings = true;
	private bool displayStarSettings = true;

	Material material;
	const float floatBoolTreshold = 0.5f;

	private static class Labels {
		public const string Sun = "Sun";
		public const string SkyGradient = "Sky Gradient";
		public const string SunHalo = "Sun Halo";
		public const string HorizonHalo = "Horizon Halo";
		public const string Clouds = "Clouds";
		public const string Environment = "Environment Map";
		public const string Stars = "Stars";
	}

	void FindProperties(MaterialProperty[] props) {
		skyGradientToggle = FindProperty ("_UseGradient", props);
		topColor = FindProperty ("_topColor", props);
		horizonColor = FindProperty ("_horizonColor", props);
		bottomColor = FindProperty ("_bottomColor", props);
		topSkyColorBlending = FindProperty ("_topSkyColorBlending", props);
		bottomSkyColorBlending = FindProperty ("_bottomSkyColorBlending", props);

		//sunToggle = FindProperty ("_UseSun", props);
		//sunColor = FindProperty ("_sunColor", props);
		//sunIntensities = FindProperty ("_sunIntensity", props);
		//sunSharpness = FindProperty ("_sunSharpness", props);

		//sunHaloToggle = FindProperty ("_UseSunHalo", props);
		//sunHaloColor = FindProperty ("_sunHaloColor", props);
		//sunHaloIntensities = FindProperty ("_sunHaloIntensity", props);
		//sunHaloSize = FindProperty ("_sunHaloSize", props);

		horizonHaloToggle = FindProperty ("_UseHorizonHalo", props);
		horizonHaloColor = FindProperty ("_horizonHaloColor", props);
		horizonHaloIntensities = FindProperty ("_horizonHaloIntensity", props);
		horizonHaloSize = FindProperty ("_horizonHaloSize", props);

		cloudToggle = FindProperty("_UseClouds", props);
		cloudColor = FindProperty("_CloudColor", props);
		cloudShadow = FindProperty("_CloudShadow", props);
		cloudLowTex = FindProperty("_CloudLowTex", props);
		cloudHighTex = FindProperty("_CloudHighTex", props);
		cloudWaveTex = FindProperty("_CloudWaveTex", props);
		cloudBending = FindProperty("_CloudBending", props);
		cloudFilter = FindProperty("_CloudFilter", props);
		cloudLowAlt = FindProperty("_CloudLowAlt", props);
		cloudHighAlt = FindProperty("_CloudHighAlt", props);
		cloudOffset = FindProperty("_CloudOffset", props);
		cloudParallax = FindProperty("_CloudParallax", props);
		cloudSpeed = FindProperty("_CloudSpeed", props);

		environmentToggle = FindProperty("_UseEnvironment", props);
		//environmentCubemap = FindProperty("_EnvironmentTex", props);
		lowEnvironmentMap = FindProperty("_LowTex", props);
		midEnvironmentMap = FindProperty("_MidTex", props);
		highEnvironmentMap = FindProperty("_HighTex", props);
		environmentBlend = FindProperty("_Blend", props);
		envColor = FindProperty("_EnvColor", props);
		hue = FindProperty("_Hue", props);
		saturation = FindProperty("_Saturation", props);
		brightness = FindProperty("_Brightness", props);
		contrast = FindProperty("_Contrast", props);
		//threshold = FindProperty("_threshhold", props);
		//slope = FindProperty("_slope", props);
		//keyColor = FindProperty("_keyingColor", props);
		starToggle = FindProperty("_UseStars", props);
		starTexture = FindProperty("_StarTex", props);
		starColor = FindProperty("_StarColor", props);
	}

	public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties) {
		material = materialEditor.target as Material;
		FindProperties (properties);
		InitMaterialUI ();

		const int defaultSpace = 5;
		ShowSkyGradientSettings (materialEditor);
//		GUILayout.Space (defaultSpace);
//		ShowSunSettings (materialEditor);
//		GUILayout.Space(defaultSpace);
//		ShowSunHaloSettings (materialEditor);
		GUILayout.Space(defaultSpace);
		ShowStarSettings(materialEditor);
		GUILayout.Space(defaultSpace);
		ShowHorizonHaloSettings (materialEditor);
		GUILayout.Space(defaultSpace);
		ShowCloudSettings(materialEditor);
		GUILayout.Space(defaultSpace);
		ShowEnvironmentSettings(materialEditor);
	}

	void InitMaterialUI() {
		displayGradientSettings = (material.IsKeywordEnabled (SKY_GRADIENT_KEYWORD) && displayGradientSettings) || displayGradientSettings;
		displaySunSettings =  (material.IsKeywordEnabled (SUN_KEYWORD) && displaySunSettings) || displaySunSettings;
		displaySunHaloSettings = (material.IsKeywordEnabled (SUN_HALO_KEYWORD) && displaySunHaloSettings) || displaySunHaloSettings;
		displayHorizonHaloSettings = (material.IsKeywordEnabled (HORIZON_HALO_KEYWORD) && displayHorizonHaloSettings) || displayHorizonHaloSettings;
		displayCloudSettings = (material.IsKeywordEnabled(CLOUD_KEYWORD) && displayCloudSettings) || displayCloudSettings;
		displayEnvironmentSettings = (material.IsKeywordEnabled(ENV_KEYWORD) && displayEnvironmentSettings) || displayEnvironmentSettings;
		displayStarSettings = (material.IsKeywordEnabled(STAR_KEYBWORD) && displayEnvironmentSettings) || displayStarSettings;
	}

	void ShowEnvironmentSettings(MaterialEditor materialEditor)
	{
		displayEnvironmentSettings = ThreeStageProceduralSky.EditorGUIHelper.Header(Labels.Environment, environmentToggle, displayEnvironmentSettings);
		bool isEnvEnabled = FloatToBool(environmentToggle.floatValue);
		if (displayEnvironmentSettings)
		{
			EditorGUI.indentLevel++;
			using (new EditorGUI.DisabledGroupScope(!isEnvEnabled))
			{
				materialEditor.ShaderProperty(environmentBlend, environmentBlend.displayName);
			//	materialEditor.ShaderProperty(environmentCubemap, environmentCubemap.displayName);
				materialEditor.ShaderProperty(lowEnvironmentMap, lowEnvironmentMap.displayName);
				materialEditor.ShaderProperty(midEnvironmentMap, midEnvironmentMap.displayName);
				materialEditor.ShaderProperty(highEnvironmentMap, highEnvironmentMap.displayName);
				materialEditor.ShaderProperty(envColor, envColor.displayName);
				materialEditor.ShaderProperty(hue, hue.displayName);
				materialEditor.ShaderProperty(saturation, saturation.displayName);
				materialEditor.ShaderProperty(brightness, brightness.displayName);
				materialEditor.ShaderProperty(contrast, contrast.displayName);
				//materialEditor.ShaderProperty(threshold, threshold.displayName);
				//materialEditor.ShaderProperty(slope, slope.displayName);
				//materialEditor.ShaderProperty(keyColor, keyColor.displayName);
			}
			EditorGUI.indentLevel--;
		}
		if (isEnvEnabled)
		{
			material.EnableKeyword(ENV_KEYWORD);
		}
		else
		{
			material.DisableKeyword(ENV_KEYWORD);
		}
	}
	void ShowCloudSettings(MaterialEditor materialEditor)
	{
		displayCloudSettings = ThreeStageProceduralSky.EditorGUIHelper.Header(Labels.Clouds, cloudToggle, displayCloudSettings);
		bool isCloudsEnabled = FloatToBool(cloudToggle.floatValue);
		if (displayCloudSettings)
		{
			EditorGUI.indentLevel++;
			using (new EditorGUI.DisabledGroupScope(!isCloudsEnabled))
			{
				materialEditor.ShaderProperty(cloudColor, cloudColor.displayName);
				materialEditor.ShaderProperty(cloudShadow, cloudShadow.displayName);
				materialEditor.ShaderProperty(cloudLowTex, cloudLowTex.displayName);
				materialEditor.ShaderProperty(cloudHighTex, cloudHighTex.displayName);
				materialEditor.ShaderProperty(cloudWaveTex, cloudWaveTex.displayName);
				materialEditor.ShaderProperty(cloudBending, cloudBending.displayName);
				materialEditor.ShaderProperty(cloudFilter, cloudFilter.displayName);
				materialEditor.ShaderProperty(cloudLowAlt, cloudLowAlt.displayName);
				materialEditor.ShaderProperty(cloudHighAlt, cloudHighAlt.displayName);
				materialEditor.ShaderProperty(cloudOffset, cloudOffset.displayName);
				materialEditor.ShaderProperty(cloudParallax, cloudParallax.displayName);
				materialEditor.ShaderProperty(cloudSpeed, cloudSpeed.displayName);
			}
			EditorGUI.indentLevel--;
		}
		if (isCloudsEnabled)
		{
			material.EnableKeyword(CLOUD_KEYWORD);
		}
		else {
			material.DisableKeyword(CLOUD_KEYWORD);
		}
	}


	void ShowSkyGradientSettings (MaterialEditor materialEditor) {
		displayGradientSettings = ThreeStageProceduralSky.EditorGUIHelper.Header (Labels.SkyGradient, skyGradientToggle, displayGradientSettings);
		bool isSkyGradientEnable = FloatToBool(skyGradientToggle.floatValue);
		if (displayGradientSettings) {
			EditorGUI.indentLevel++;
			using (new EditorGUI.DisabledGroupScope (!isSkyGradientEnable))	{
				materialEditor.ShaderProperty (topColor, topColor.displayName);
				materialEditor.ShaderProperty (horizonColor, horizonColor.displayName);
				materialEditor.ShaderProperty (bottomColor, bottomColor.displayName);
				materialEditor.ShaderProperty (topSkyColorBlending, topSkyColorBlending.displayName);
				materialEditor.ShaderProperty (bottomSkyColorBlending, bottomSkyColorBlending.displayName);
			}
			EditorGUI.indentLevel--;
		}
		if (isSkyGradientEnable) {
			material.EnableKeyword (SKY_GRADIENT_KEYWORD);
		} else	{
			material.DisableKeyword (SKY_GRADIENT_KEYWORD);
		}
	}

//	void ShowSunSettings (MaterialEditor materialEditor) {
//		displaySunSettings = ThreeStageProceduralSky.EditorGUIHelper.Header (Labels.Sun, sunToggle, displaySunSettings);
//		bool isSunEnable = FloatToBool(sunToggle.floatValue);
//		if (displaySunSettings)	{
//			EditorGUI.indentLevel++;
//			using (new EditorGUI.DisabledGroupScope (!isSunEnable))	{
//				materialEditor.ShaderProperty (sunColor, sunColor.displayName);
//				materialEditor.ShaderProperty (sunIntensities, sunIntensities.displayName);
//				materialEditor.ShaderProperty (sunSharpness, sunSharpness.displayName);
//			}
//			EditorGUI.indentLevel--;
//		}
//		if (isSunEnable) {
//			material.EnableKeyword (SUN_KEYWORD);
//		} else	{
//			material.DisableKeyword (SUN_KEYWORD);
//		}
//	}

//	void ShowSunHaloSettings (MaterialEditor materialEditor) {
//		displaySunHaloSettings = ThreeStageProceduralSky.EditorGUIHelper.Header (Labels.SunHalo, sunHaloToggle, displaySunHaloSettings);
//		bool isSunHaloEnable = FloatToBool(sunHaloToggle.floatValue);
//		if (displaySunHaloSettings)	{
//			EditorGUI.indentLevel++;
//			using (new EditorGUI.DisabledGroupScope (!isSunHaloEnable))	{
//				materialEditor.ShaderProperty (sunHaloColor, sunHaloColor.displayName);
//				materialEditor.ShaderProperty (sunHaloIntensities, sunHaloIntensities.displayName);
//				materialEditor.ShaderProperty (sunHaloSize, sunHaloSize.displayName);
//			}
//			EditorGUI.indentLevel--;
//		}
//		if (isSunHaloEnable) {
//			material.EnableKeyword (SUN_HALO_KEYWORD);
//		} else {
//			material.DisableKeyword (SUN_HALO_KEYWORD);
//		}
//	}

	void ShowHorizonHaloSettings (MaterialEditor materialEditor) {
		displayHorizonHaloSettings = ThreeStageProceduralSky.EditorGUIHelper.Header (Labels.HorizonHalo, horizonHaloToggle, displayHorizonHaloSettings);
		bool isHorizonHaloEnable = FloatToBool(horizonHaloToggle.floatValue);
		if (displayHorizonHaloSettings) {
			EditorGUI.indentLevel++;
			using (new EditorGUI.DisabledGroupScope (!isHorizonHaloEnable))	{
				materialEditor.ShaderProperty (horizonHaloColor, horizonHaloColor.displayName);
				materialEditor.ShaderProperty (horizonHaloIntensities, horizonHaloIntensities.displayName);
				materialEditor.ShaderProperty (horizonHaloSize, horizonHaloSize.displayName);
			}
			EditorGUI.indentLevel--;
		}
		if (isHorizonHaloEnable) {
			material.EnableKeyword (HORIZON_HALO_KEYWORD);
		} else	{
			material.DisableKeyword (HORIZON_HALO_KEYWORD);
		}
	}

	void ShowStarSettings(MaterialEditor materialEditor)
	{
		displayStarSettings = ThreeStageProceduralSky.EditorGUIHelper.Header(Labels.Stars, starToggle, displayStarSettings);
		bool isStarEnable = FloatToBool(starToggle.floatValue);
		if (displayStarSettings)
		{
			EditorGUI.indentLevel++;
			using (new EditorGUI.DisabledGroupScope(!isStarEnable))
			{
				materialEditor.ShaderProperty(starTexture, starTexture.displayName);
				materialEditor.ShaderProperty(starColor, starColor.displayName);
			}
			EditorGUI.indentLevel--;
		}
		if (isStarEnable)
		{
			material.EnableKeyword(STAR_KEYBWORD);
		}
		else
		{
			material.DisableKeyword(STAR_KEYBWORD);
		}
	}

	private bool FloatToBool(float number) {
		return number >= floatBoolTreshold;
	}
}
