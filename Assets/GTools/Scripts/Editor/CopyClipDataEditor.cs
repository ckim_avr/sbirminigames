﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using GTools.Utils;
using UnityEngine.Timeline;
using GTools.TimelineExtensions.Playables;
using System.Linq;
using GTools.Utils.Extensions;

public class CopyClipDataEditor : EditorWindow
{
	public string copybuffer;
	public object source;
	public object dest;


	public Object selected;
	public string dataName;

	Vector2 scrollPos;
	Vector2 innerScrollPos;

	[MenuItem("GTools/Copy Clip Data")]
	static void CopyClipData()
	{
		CopyClipDataEditor window = EditorWindow.GetWindow<CopyClipDataEditor>();
		window.Show();
	}


	private Vector2 GetContentSize(string s)
	{
		GUIContent content = new GUIContent(s);
		GUIStyle style = GUI.skin.box;
		style.alignment = TextAnchor.UpperLeft;

		Vector2 size = style.CalcSize(content);
		size.y = style.CalcHeight(content, size.y);

		return size;
	}

	private void OnGUI()
	{
		if (EditorGUIUtility.systemCopyBuffer != copybuffer)
		{
			dataName = "N/A";
		}
		copybuffer = EditorGUIUtility.systemCopyBuffer;
		selected = Selection.activeObject;
		//GUI THINGS
		EditorGUILayout.LabelField(string.Format("Copy Buffer: {0}", dataName));
		EditorGUILayout.BeginHorizontal();
		Vector2 size = GetContentSize(copybuffer);
		scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(position.width / 3f), GUILayout.Height(position.height / 1.5f));
		EditorGUILayout.TextArea(copybuffer, GUILayout.Height(size.y));
		EditorGUILayout.EndScrollView();
		EditorGUILayout.BeginVertical();
		if (selected != null)
		{
			var fi = ReflectionUtils.GetAllFields(selected.GetType());
			EditorGUILayout.ObjectField(selected, typeof(Object), true);
			foreach (var item in fi)
			{
				if (item.Name == "m_Item")
				{
					EditorGUILayout.LabelField("Timeline Clip Asset:");
					var clip = item.GetValue(selected) as TimelineClip;
					var asset = clip.asset;
					var data = asset.GetType().GetFields();
					foreach (var i in data)
					{
						#region Supported Data Types
						if (i.Name == "skyboxData")
						{
							EditorGUILayout.LabelField("Skybox Data");
							var playableData = i.GetValue(asset) as SkyboxThreeStageDataPlayable;
							var playableFields = ReflectionUtils.GetAllFields(playableData.GetType());
							innerScrollPos = EditorGUILayout.BeginScrollView(innerScrollPos, GUILayout.Width(600), GUILayout.Height(position.height / 2f));
							foreach (var j in playableFields)
							{

								EditorGUILayout.BeginHorizontal();
								EditorGUILayout.LabelField(j.Name);
								EditorGUILayout.LabelField(j.GetValue(playableData).ToString());
								EditorGUILayout.EndHorizontal();
							}
							EditorGUILayout.EndScrollView();
							if (GUILayout.Button("Copy Values"))
							{
								source = playableData;
								string json = JsonUtility.ToJson(source, true);
								copybuffer = json;
								EditorGUIUtility.systemCopyBuffer = json;
								dataName = "Skybox Data";
								ShowNotification(new GUIContent("Copied Data"));
							}
							if (GUILayout.Button("Paste Values"))
							{
								object paste = JsonUtility.FromJson(copybuffer, source.GetType());
								dest = playableData;
								ExtensionMethods.MapAllFields(paste, dest);
							}
						}
						if (i.Name == "hsbData")
						{
							EditorGUILayout.LabelField("HSBObject Data");
							var playableData = i.GetValue(asset) as HSBThreeStageDataPlayable;
							var playableFields = ReflectionUtils.GetAllFields(playableData.GetType());
							innerScrollPos = EditorGUILayout.BeginScrollView(innerScrollPos, GUILayout.Width(600), GUILayout.Height(position.height / 2f));
							foreach (var j in playableFields)
							{

								EditorGUILayout.BeginHorizontal();
								EditorGUILayout.LabelField(j.Name);
								EditorGUILayout.LabelField(j.GetValue(playableData).ToString());
								EditorGUILayout.EndHorizontal();
							}
							EditorGUILayout.EndScrollView();
							if (GUILayout.Button("Copy Values"))
							{
								source = playableData;
								string json = JsonUtility.ToJson(source, true);
								copybuffer = json;
								EditorGUIUtility.systemCopyBuffer = json;
								dataName = "HSBObject Data";
								ShowNotification(new GUIContent("Copied Data"));
							}
							if (GUILayout.Button("Paste Values"))
							{
								object paste = JsonUtility.FromJson(copybuffer, source.GetType());
								dest = playableData;
								ExtensionMethods.MapAllFields(paste, dest);
							}
						}
					//	if (i.Name == "animData")
					//	{
					//		EditorGUILayout.LabelField("Animation Scaler Data");
					//		var playableData = i.GetValue(asset) as AnimationScalerDataPlayable;
					//		var playableFields = ReflectionUtils.GetAllFields(playableData.GetType());
					//		innerScrollPos = EditorGUILayout.BeginScrollView(innerScrollPos, GUILayout.Width(600), GUILayout.Height(position.height / 2f));
					//		foreach (var j in playableFields)
					//		{
					//
					//			EditorGUILayout.BeginHorizontal();
					//			EditorGUILayout.LabelField(j.Name);
					//			EditorGUILayout.LabelField(j.GetValue(playableData).ToString());
					//			EditorGUILayout.EndHorizontal();
					//		}
					//		EditorGUILayout.EndScrollView();
					//		if (GUILayout.Button("Copy Values"))
					//		{
					//			source = playableData;
					//			string json = JsonUtility.ToJson(source, true);
					//			copybuffer = json;
					//			EditorGUIUtility.systemCopyBuffer = json;
					//			dataName = "Animation Scaler Data";
					//			ShowNotification(new GUIContent("Copied Data"));
					//		}
					//		if (GUILayout.Button("Paste Values"))
					//		{
					//			object paste = JsonUtility.FromJson(copybuffer, source.GetType());
					//			dest = playableData;
					//			ExtensionMethods.MapAllFields(paste, dest);
					//		}
					//	}
					//	if (i.Name == "waterData")
					//	{
					//		EditorGUILayout.LabelField("Water Data");
					//		var playableData = i.GetValue(asset) as WaterControllerDataPlayable;
					//		var playableFields = ReflectionUtils.GetAllFields(playableData.GetType());
					//		innerScrollPos = EditorGUILayout.BeginScrollView(innerScrollPos, GUILayout.Width(600), GUILayout.Height(position.height / 2f));
					//		foreach (var j in playableFields)
					//		{
					//
					//			EditorGUILayout.BeginHorizontal();
					//			EditorGUILayout.LabelField(j.Name);
					//			EditorGUILayout.LabelField(j.GetValue(playableData).ToString());
					//			EditorGUILayout.EndHorizontal();
					//		}
					//		EditorGUILayout.EndScrollView();
					//		if (GUILayout.Button("Copy Values"))
					//		{
					//			source = playableData;
					//			string json = JsonUtility.ToJson(source, true);
					//			copybuffer = json;
					//			EditorGUIUtility.systemCopyBuffer = json;
					//			dataName = "Water Data";
					//			ShowNotification(new GUIContent("Copied Data"));
					//		}
					//		if (GUILayout.Button("Paste Values"))
					//		{
					//			object paste = JsonUtility.FromJson(copybuffer, source.GetType());
					//			dest = playableData;
					//			ExtensionMethods.MapAllFields(paste, dest);
					//		}
					//	}
					//	if (i.Name == "complexityData")
					//	{
					//		EditorGUILayout.LabelField("Complexity Data");
					//		var playableData = i.GetValue(asset) as ComplexityDataPlayable;
					//		var playableFields = ReflectionUtils.GetAllFields(playableData.GetType());
					//		innerScrollPos = EditorGUILayout.BeginScrollView(innerScrollPos, GUILayout.Width(600), GUILayout.Height(position.height / 2f));
					//		foreach (var j in playableFields)
					//		{
					//
					//			EditorGUILayout.BeginHorizontal();
					//			EditorGUILayout.LabelField(j.Name);
					//			EditorGUILayout.LabelField(j.GetValue(playableData).ToString());
					//			EditorGUILayout.EndHorizontal();
					//		}
					//		EditorGUILayout.EndScrollView();
					//		if (GUILayout.Button("Copy Values"))
					//		{
					//			source = playableData;
					//			string json = JsonUtility.ToJson(source, true);
					//			copybuffer = json;
					//			EditorGUIUtility.systemCopyBuffer = json;
					//			dataName = "Complexity Data";
					//			ShowNotification(new GUIContent("Copied Data"));
					//		}
					//		if (GUILayout.Button("Paste Values"))
					//		{
					//			object paste = JsonUtility.FromJson(copybuffer, source.GetType());
					//			dest = playableData;
					//			ExtensionMethods.MapAllFields(paste, dest);
					//		}
					//
					//	}
						#endregion
					}
				}
			}
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();
		}
	}

	private void OnInspectorUpdate()
	{
		this.Repaint();
	}
}
