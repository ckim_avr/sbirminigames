﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine.Timeline;
using GTools.Object.Properties;
using GTools.TimelineExtensions.Playables;
using UnityEngine.Playables;
using System.Linq;

[CustomEditor(typeof(SkyboxThreeStageData))]
public class SkyboxThreeStageDataEditor : Editor {

	private SkyboxThreeStageObject myObject;
	private PlayableDirector myDirector;
	private TimelineClip myClip;

	public override void OnInspectorGUI ()
	{
		EditorGUI.BeginChangeCheck();
		DrawDefaultInspector();

		if( EditorGUI.EndChangeCheck() )
		{
			UpdateValues();
		}

		#if OSC_REALTIME
		if( UnityEngine.GUILayout.Button( "Update Remote Data" ) )
		{
			UpdateValues();
		}
		#endif
	}

	private void UpdateValues()
	{
		#if !OSC_REALTIME || !UNITY_EDITOR
		return;
		#endif
		SkyboxThreeStageData targetData = target as SkyboxThreeStageData;
		if( myObject == null )
			myObject = FindMyObject(targetData);

		if( myObject != null && WithinDirectorBounds() )
		{
			SkyboxThreeStageDataPlayable skyboxData = targetData.skyboxData;
			myObject.SkyGradient = skyboxData.skyGradient;
			myObject.Stars = skyboxData.stars;
			myObject.HorizonHalo = skyboxData.horizonHalo;
			myObject.Clouds = skyboxData.clouds;
			myObject.EnvironmentMap = skyboxData.environmentMap;

			myObject.TopColor = skyboxData.topColor;
			myObject.HorizonColor = skyboxData.horizonColor;
			myObject.BottomColor = skyboxData.bottomColor;
			myObject.TopSkyColorBlending = skyboxData.topSkyColorBlending;
			myObject.BottomSkyColorBlending = skyboxData.bottomSkyColorBlending;
			myObject.StarColor = skyboxData.starColor;
			myObject.HorizonHaloColor = skyboxData.horizonHaloColor;
			myObject.HorizonHaloIntensity = skyboxData.horizonHaloIntensity;
			myObject.HorizonHaloSize = skyboxData.horizonHaloSize;
			myObject.CloudColor = skyboxData.cloudColor;
			myObject.CloudShadow = skyboxData.cloudShadow;
			myObject.CloudBending = skyboxData.cloudBending;
			myObject.CloudFilter = skyboxData.cloudFilter;
			myObject.LowAlt = skyboxData.lowAlt;
			myObject.HighAlt = skyboxData.highAlt;
			myObject.CloudOffset = skyboxData.cloudOffset;
			myObject.CloudParallax = skyboxData.cloudParallax;
			myObject.CloudSpeed = skyboxData.cloudSpeed;
			myObject.EnvironmentBlend = skyboxData.environmentBlend;

			myObject.UpdateValues();
			myObject.InvokeOnValuesUpdated();
		}
	}

	private SkyboxThreeStageObject FindMyObject( SkyboxThreeStageData targetData )
	{
		myDirector = targetData.owner.GetComponent<PlayableDirector>();
		if( myDirector.playableAsset is TimelineAsset )
		{
			// Get tracks that are HSB tracks
			IEnumerable<TrackAsset> tracks = ((TimelineAsset)(myDirector.playableAsset)).GetOutputTracks().Where( track => track is GTools.SkyboxThreeStageTrack );
			foreach( TrackAsset track in tracks )
			{
				// Go through clips in tracks and find myself in them
				var clips = track.GetClips();
				foreach( TimelineClip clip in clips )
				{
					if( clip.asset == targetData )
					{
						myClip = clip;
						return myDirector.GetGenericBinding( track ) as SkyboxThreeStageObject;
					}
				}
			}
		}
		return null;
	}

	private bool WithinDirectorBounds()
	{
		if( myClip == null )
			return false;
		if( myDirector == null )
			(target as SkyboxThreeStageData).owner.GetComponent<PlayableDirector>();

		double currTime = myDirector.time;
		if( currTime >= myClip.start && currTime <= myClip.end )
		{
			return true;
		}
		return false;
	}
}
