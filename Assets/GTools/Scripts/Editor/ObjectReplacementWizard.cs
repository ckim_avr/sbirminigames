﻿using UnityEngine;
using UnityEditor;


namespace GTools.Utils.Editor
{
	public class ObjectReplacementWizard : ScriptableWizard
	{
		public bool copyValues = true;
		public GameObject newObj;
		public GameObject[] OldObjects;
	
		[MenuItem("GTools/Replace GameObjects")]
	
	
	
		static void CreateWizard()
		{
			var replacementGameObjects = ScriptableWizard.DisplayWizard<ObjectReplacementWizard>("Replace GameObjects", "Replace");
			replacementGameObjects.OldObjects = Selection.gameObjects;
		}
	
		private void OnWizardCreate()
		{
	
			foreach (GameObject go in OldObjects)
			{
				GameObject m_GO;
				m_GO = (GameObject)PrefabUtility.InstantiatePrefab(newObj);
				m_GO.transform.parent = go.transform.parent;
				m_GO.transform.position = go.transform.position;
				m_GO.transform.rotation = go.transform.rotation;
				m_GO.transform.localScale = go.transform.localScale;
	
				DestroyImmediate(go);
			}
		}
	
	}
}
