﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace ThreeStageProceduralSky {

	public static class EditorGUIHelper {

		public static GUIStyle header;
		public static GUIStyle headerCheckbox;

		static EditorGUIHelper() {

			header = new GUIStyle ("ShurikenModuleTitle") {
				font = (new GUIStyle ("Label")).font,
				border = new RectOffset (15, 7, 4, 4),
				fixedHeight = 22,
				contentOffset = new Vector2 (20f, -2f)
			};

			headerCheckbox = new GUIStyle("ShurikenCheckMark");
		}

		public static bool Header(string title, MaterialProperty enabled, bool isExpanded){

			Event e = Event.current;
			bool display = isExpanded;

			Rect rectHeader = GUILayoutUtility.GetRect(16f, 22f, EditorGUIHelper.header);
			GUI.Box (rectHeader, title, EditorGUIHelper.header);

			Rect rectToggler = new Rect(rectHeader.x + 4f, rectHeader.y + 4f, 13f, 13f);
			if (e.type == EventType.Repaint) {			
				headerCheckbox.Draw(rectToggler, false, false, (enabled.floatValue == 1) , false);
			}

			if (e.type == EventType.MouseDown) {

				const float kOffset = 2f;
				rectToggler.x -= kOffset;
				rectToggler.y -= kOffset;
				rectToggler.width += kOffset * 2f;
				rectToggler.height += kOffset * 2f;

				if (rectToggler.Contains (e.mousePosition))
				{
					enabled.floatValue = (enabled.floatValue >= 0.5f) ? 0f : 1f;
					e.Use();
				} else if (rectHeader.Contains (e.mousePosition))
				{
					display = !isExpanded;
					e.Use();
				}
			}

			return display;

		}
	}
}
