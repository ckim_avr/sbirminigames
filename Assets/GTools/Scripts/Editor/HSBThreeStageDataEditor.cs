﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Timeline;
using GTools.Object.Properties;
using GTools.TimelineExtensions.Playables;
using UnityEngine.Playables;
using System.Linq;

[CustomEditor(typeof(HSBThreeStageData))]
public class HSBThreeStageDataEditor : Editor {

	private HSBThreeStageObject myObject;
	private PlayableDirector myDirector;
	private TimelineClip myClip;

	public override void OnInspectorGUI ()
	{
		var hsbTarget = target as HSBThreeStageData;
		EditorGUI.BeginChangeCheck();
		DrawDefaultInspector();

		EditorGUI.BeginDisabledGroup(true);
		EditorGUILayout.IntField("Clip ID",hsbTarget.clipID);
		EditorGUI.EndDisabledGroup();

		if( EditorGUI.EndChangeCheck() )
		{
			UpdateValues();
		}

		#if OSC_REALTIME
		if( GUILayout.Button( "Update Remote Data" ) )
		{
			UpdateValues();
		}
		#endif
	}

	private void UpdateValues()
	{
		#if !OSC_REALTIME || !UNITY_EDITOR
		return;
		#endif
		HSBThreeStageData targetData = target as HSBThreeStageData;
		if( myObject == null )
			myObject = FindMyObject(targetData);
		
		if( myObject != null /*&& WithinDirectorBounds()*/ )
		{
			HSBThreeStageDataPlayable hsbData = targetData.hsbData;
			myObject.Color		= hsbData.color;
			myObject.Blend		= hsbData.blend;
			myObject.Hue		= hsbData.hue;
			myObject.Saturation	= hsbData.saturation;
			myObject.Brightness	= hsbData.brightness;
			myObject.Contrast	= hsbData.contrast;
			
			myObject.UpdateValues();
			myObject.InvokeOnValuesUpdated(targetData.clipID);
		}
	}

	private HSBThreeStageObject FindMyObject( HSBThreeStageData targetData )
	{
		myDirector = targetData.owner.GetComponent<PlayableDirector>();
		if( myDirector.playableAsset is TimelineAsset )
		{
			// Get tracks that are HSB tracks
			IEnumerable<TrackAsset> tracks = ((TimelineAsset)(myDirector.playableAsset)).GetOutputTracks().Where( track => track is GTools.HSBThreeStageTrack );
			foreach( TrackAsset track in tracks )
			{
				// Go through clips in tracks and find myself in them
				var clips = track.GetClips();
				foreach( TimelineClip clip in clips )
				{
					if( clip.asset == targetData )
					{
						myClip = clip;
						return myDirector.GetGenericBinding( track ) as HSBThreeStageObject;
					}
				}
			}
		}
		return null;
	}

	//private bool WithinDirectorBounds()
	//{
	//	if( myClip == null )
	//		return false;
	//	if( myDirector == null )
	//		(target as HSBThreeStageData).owner.GetComponent<PlayableDirector>();

	//	double currTime = myDirector.time;
	//	if( currTime >= myClip.start && currTime <= myClip.end )
	//	{
	//		return true;
	//	}
	//	return false;
	//}
}
