﻿using SBIR.MiniGames;

namespace SBIR.Utils
{
	public interface IGazeInteractable
	{
		ObjectType ObjectType { get; set; }
		void OnGazeEnter();
		void OnGazeExit();
	}
}
