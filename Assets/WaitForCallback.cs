﻿using UnityEngine;
using System;

public class WaitForCallback<T>  : CustomYieldInstruction
{
	private bool done;

	public WaitForCallback(Action<Action<T>> callCallback)
	{
		callCallback(r => { Result = r; done = true; });
	}

	public override bool keepWaiting {get{ return done == false;}}

	public T Result { get; private set; }
}

